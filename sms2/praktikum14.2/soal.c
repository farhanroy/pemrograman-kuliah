#include<stdio.h>
#include<stdlib.h>

struct node {
    char data;
    struct node* left;
    struct node* right;
    struct node* next;
};

struct node* createNode(char);
struct node* pop();
void push(struct node*);

void printPreorder (struct node*);
void printInorder (struct node*);
void printPostorder (struct node*);

int isOperator(char);

struct node* head;

int main() {
    head = NULL;

    char data[] = {'(', '9', '-', '5', ')', '*', '(', '7', '+', '4', ')'};
    int n = sizeof(data) / sizeof(data[0]);
    printf("%d\n", n);
    struct node *operator, *operand, *root;

    for (int i = 0; i < n; i++) {
        if (data[i] == '(') {
            operator = createNode(data[i]);
            push(operator);
        } else if (data[i] == ')') {
            operator = pop();
            root = createNode(operator->data);
            root->right = pop();
            root->left = pop();
            push(operator);
        } else if (isOperator(data[i])) {
            operator = createNode(data[i]);
            push(operator);
        } else {
            operand = createNode(data[i]);
            push(operand);
        }
    }

    printPreorder(root);

    return 0;
}

struct node* createNode(char data) {
    struct node* newNode = (struct node *) malloc(sizeof(struct node));
    newNode->data = data;
    newNode->left = NULL;
    newNode->right = NULL;
    newNode->next = NULL;
    return (newNode); 
}

void push(struct node* x) {
    if (head == NULL) {
        head = x;
    } else {
        (x)->next = head;
        head = x;
    }
}

struct node* pop() {
    struct node* p = head;
    head = head->next;
    return p;
}

void printPreorder (struct node* root) {
    if (root != NULL) {
        printf("%c", root->data);
        printPreorder(root->left);
        printPreorder(root->right);
    }
}

void printInorder (struct node* root) {
    if (root != NULL) {
        printInorder(root->left);
        printf("%c", root->data);
        printInorder(root->right);
    }
}

void printPostorder (struct node* root) {
    if (root != NULL) {
        printPostorder(root->left);
        printPostorder(root->right);
        printf("%c", root->data);
    }
}

int isOperator(char x) {
    return x == '+' || x == '-' || x == '*' || x == '/';
}