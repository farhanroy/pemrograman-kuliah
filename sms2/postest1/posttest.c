#include<stdio.h>
#include<stdlib.h>

typedef struct node Node;

struct node {
    int data;
    Node *next;
};

Node *head;
Node *newNode;

void chooseMenu();
void processMenu(int);

void exitProgram();

void createNewNode();
void deleteNode(Node *);
void getAllItems();
void insertItem();
void deleteItem();
void searchItem();

void deleteItemAtFirst(Node *);

int main() {
    head = NULL;
    chooseMenu();
    return 0;
}

void chooseMenu() {
    int menu;
    printf("Menu Insert\n");
    printf("1. Penambahan secara terurut\n");
    printf("2. Pencarian data\n");
    printf("3. Penghapusan data\n");
    printf("4. Keluar\n");
    printf("Pilih menu: ");
    scanf("%d", &menu);
    processMenu(menu);
}

void processMenu(int menu) {

    switch(menu) {
        case 1:
            insertItem();
            break;
        case 2:
            searchItem();
            break;
        case 3: 
            deleteItem();
            break;
        default:
            exitProgram();
    }
}

void exitProgram() {
    printf("\nExit from program");
    exit(0);
}

void createNewNode() {
    newNode = (Node *) malloc(sizeof(Node));
    
    if (newNode == NULL) {
        printf("Cannot allocate memory");
        exit(0);
    } else {
        printf("Enter value for new node: ");
        scanf("%d", &newNode->data);
        newNode->next = NULL;
    }
}

void getAllItems() {
    Node *temp = head;
    while (temp != NULL) {
        printf("%d \n", temp->data);
        temp = temp->next;
    }
}

void insertItem() {
    Node *current;
    createNewNode();
    if (head == NULL || head->data >= newNode->data) {
        newNode->next = head;
        head = newNode;
    } else {
        current = head;
        while(current->next != NULL && current->next->data < newNode->data) {
            current = current->next;
        }
        newNode->next = current->next;
        current->next = newNode;

    }
    getAllItems();
    chooseMenu();
}

void searchItem() {
    int key, counter = 0;
    Node *temp;
    
    temp = head;

    printf("\nSearch key: ");
    fflush(stdin);
    scanf("%d", &key);

    while (temp != NULL) {

        if (temp->data == key) {
            counter++;
        }
        temp = temp->next;
    }

    if (counter > 0) {
        printf("\n data %d ada %d kali\n", key, counter);
    } else {
        printf("\ndata tidak ditemukan\n");
    }
    chooseMenu();
}

void deleteItem() {
    int key, counter = 0;
    Node *current = head, *temp;

    printf("\nSearch key: ");
    scanf("%d", &key);

    if (head == NULL) {
        chooseMenu();
    } else if (head->data == key) {
        deleteItemAtFirst(current);
    } else {
        while (current->data != key) {
            if (current->next == NULL) {
                exitProgram();
            } else {
                temp = current;
                current = current->next;
            }
        }
        temp->next = current->next;
    }

    deleteNode(current);

    getAllItems();
    chooseMenu();
}

void deleteItemAtFirst(Node *current) {
    if(head->next == NULL) {
        head = NULL;
    } else {
        head = current->next;
    }
}

void deleteNode(Node *delete) {
    free(delete);
    delete = NULL;
}