#include<stdio.h>
#include<stdlib.h>

typedef struct node Node;

struct node {
    int data;
    Node *next;
};

Node *head;
Node *newNode;

void chooseMenu();

void createNewNode();
void freeNode(Node *);
void getAllNode();

void insertLast();
void deleteFirst();

void programExit();

int main() {
    head = NULL;
    chooseMenu();
}

void chooseMenu() {
    int menu;
    printf("Menu:\n");
    printf("1. Enqueue\n2. Dequeue\n3. Get All\n4. Exit\n");
    printf("Pilih menu: ");
    scanf("%d", &menu);

    if (menu == 1) {
        insertLast();
    } else if (menu == 2) {
        deleteFirst();
    } else if (menu == 3) {
        getAllNode();
    } else {
        programExit();
    }
}

void createNewNode() {
    newNode = (Node *) malloc(sizeof(Node));
    
    if (newNode == NULL) {
        printf("Cannot allocate memory");
        exit(0);
    } else {
        printf("Enter value for new node: ");
        scanf("%d", &newNode->data);
        newNode->next = NULL;
    }
}

void freeNode(Node *delete) {
    free(delete);
    delete = NULL;
}

void getAllNode() {
    Node *temp = head;
    while (temp != NULL) {
        printf("%d \n", temp->data);
        temp = temp->next;
    }
}

void insertLast() {
    Node *current;

    createNewNode();

    if (head == NULL) {
        head = newNode;
    } else {
        current = head;
        while(current->next != NULL) {
            current = current->next;
        }
        // add newNode in current->next
        current->next = newNode;
        // change current state to last node
        current = current->next;
    }

    getAllNode();
    chooseMenu();
}


void deleteFirst() {
    Node *current;
    current = head;
    if (head == NULL) {
        programExit();
    } else if(head->next == NULL) {
        head = NULL;
    } else {
        head = current->next;
    }

    freeNode(current);

    getAllNode();
    chooseMenu();
}

void programExit() {
    printf("\nProgram keluar\n");
    exit(0);
}