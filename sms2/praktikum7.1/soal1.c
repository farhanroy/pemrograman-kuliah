#include <stdio.h>
#include <stdlib.h>

#define MAX 3

typedef char type;
typedef struct {
    type item[MAX];
    int count;
    int front;
    int rear;
} Queue;

void initialize(Queue *);
int isFull(Queue *);
int isEmpty(Queue *);
void enqueue(Queue *);
void dequeue(Queue *);
void getAllQueue(Queue *);
void chooseMenu(Queue *queue);

int main() {
    Queue queue;
    initialize(&queue);
    chooseMenu(&queue);
    return 0;
}

void chooseMenu(Queue *queue) {
    int menu;
    printf("Menu:\n1. Enqueue Data\n2. Dequeue Data\n3. Get All\n4. Exit\n");
    printf("Pilihan: ");
    scanf("%d", &menu);
    switch(menu) {
        case 1:
            enqueue(queue);
            break;
        case 2:
            dequeue(queue);
            break;
        case 3:
            getAllQueue(queue);
            break;
        default:
            exit(0);
    }   
}

void initialize(Queue *queue) {
    queue->count = 0;
    queue->front = 0;
    queue->rear = 0;
}

int isFull(Queue *queue) {
    return queue->count == MAX;
}

int isEmpty(Queue *queue) {
    return queue->count == 0;
}

void enqueue(Queue *queue) {
    if (isFull(queue)) {
        printf("Full\n");
    } else {
        printf("Enter data: ");
        fflush(stdin);
        scanf("%c", &queue->item[queue->rear]);
        queue->rear = (queue->rear+1) % MAX;
        queue->count++;
    }
    getAllQueue(queue);
    chooseMenu(queue);
}

void dequeue(Queue *queue) {
    if (isEmpty(queue)) {
        printf("Queue sedang kosong");
    } else {
        type temp = queue->item[queue->front];
        queue->front = (queue->front+1) % MAX;
        queue->count--;
    }
    getAllQueue(queue);
    chooseMenu(queue);
}

void getAllQueue(Queue *queue) {
     int front = queue->front, rear = queue->rear;
    if (isEmpty(queue)) {
        printf("Kosong\n");
    } else {
        do {
            printf("%c\n", queue->item[front]);
            front = (front + 1) % MAX;
        } while(front != rear);
    }
    printf("\n");
    chooseMenu(queue);
}