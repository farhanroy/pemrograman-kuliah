#include<stdio.h>
#include<time.h>

int hitungFpb(int, int);

int main() {
    double time_taken;
    clock_t t;
    
    int a, b;
    printf("Masukan a: ");
    scanf("%d", &a);
    printf("Masukan b: ");
    scanf("%d", &b);

    t = clock();
    printf("Hasilnya  = %d", hitungFpb(a, b));
    t = clock() - t;
    time_taken = ((double)t)/CLOCKS_PER_SEC; 
    printf("\ntook %f seconds to execute \n", time_taken);

    return 0;
}

int hitungFpb(int a, int b) {
    if (a == 0) {
        return b;
    } else {
        return hitungFpb(b%a, a);
    }
}