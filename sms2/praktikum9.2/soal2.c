#include<stdio.h>
#include<time.h>

int main() {
    double time_taken;
    clock_t t;

    int n;
    printf("Masukan n: ");
    scanf("%d", &n);

    t = clock();

    int a = 0, b = 1, c = a+b;

    printf("%d %d ", a, b);

    for (int i = 0; i < n-2; i++) {
        printf("%d ", c);
        a = b;
        b = c;
        c = a+b;
    }

    t = clock() - t;
    time_taken = ((double)t)/CLOCKS_PER_SEC; 
    printf("\ntook %f seconds to execute \n", time_taken);

    return 0;
}