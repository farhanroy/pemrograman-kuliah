#include<stdio.h>
#include<stdlib.h>
#include<time.h>

void chooseMenu();

void iterasiFibo();
void rekursiFibo();

int hitungFibo();

int main() {
    chooseMenu();
    return 0;
}

void chooseMenu() {
    int menu;

    printf("Menu:\n1. Iterasi Fibonaci\n2. Rekursi Fibonaci\n3. Exit\n");
    printf("Pilih menu: ");
    scanf("%d", &menu);

    switch(menu) {
        case 1:
            iterasiFibo();
            break;
        case 2:
            rekursiFibo();
            break;
        default:
            exit(0);
            break;
    }
}

void iterasiFibo() {
    double time_taken;
    clock_t t;

    int n;
    printf("Masukan n: ");
    scanf("%d", &n);

    t = clock();

    int a = 0, b = 1, c = a+b;

    printf("%d %d ", a, b);

    for (int i = 0; i < n-2; i++) {
        printf("%d ", c);
        a = b;
        b = c;
        c = a+b;
    }

    t = clock() - t;
    time_taken = ((double)t)/CLOCKS_PER_SEC; 
    printf("\ntook %f seconds to execute \n", time_taken);
    chooseMenu();
}

void rekursiFibo() {
    double time_taken;
    clock_t t;
    
    int n;
    printf("Masukan n: ");
    scanf("%d", &n);

    t = clock();
    for(int i = 0; i < n; i++) {
        printf("%d ", hitungFibo(i));
    }
    t = clock() - t;
    time_taken = ((double)t)/CLOCKS_PER_SEC; 
    printf("\ntook %f seconds to execute \n", time_taken);
    chooseMenu();
}

int hitungFibo(int n) {

    if (n <=  1) {
        return n;
    } else {
        return hitungFibo(n-1) + hitungFibo(n-2);
    }
}