#include<stdio.h>
#include<time.h>

int isPrime(int, int);

int main() {
    double time_taken;
    clock_t t;

    int n;
    printf("Masukan n: ");
    scanf("%d", &n);

    t = clock();

    if (isPrime(n, 2) == 0) {
        printf("%d adalah bilangan prima", n);
    } else {
        printf("%d adalah bukan bilangan prima", n);
    }
    t = clock() - t;
    time_taken = ((double)t)/CLOCKS_PER_SEC; 
    printf("\ntook %f seconds to execute \n", time_taken);
    return 0;
}

int isPrime(int n, int i)
{
    if(n==i)
        return 0;
    else {
        if(n%i==0)
            return 1;
        else 
            return isPrime(n, i+1);
    }
}