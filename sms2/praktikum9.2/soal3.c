#include<stdio.h>
#include<time.h>

int hitungFibo(int);

int main() {
    double time_taken;
    clock_t t;

    int n;
    printf("Masukan n: ");
    scanf("%d", &n);

    t = clock();

    for(int i = 0; i < n; i++) {
        printf("%d ", hitungFibo(i));
    }

    t = clock() - t;
    time_taken = ((double)t)/CLOCKS_PER_SEC; 
    printf("\ntook %f seconds to execute \n", time_taken);
    
    return 0;
}

int hitungFibo(int n) {

    if (n <=  1) {
        return n;
    } else {
        return hitungFibo(n-1) + hitungFibo(n-2);
    }
}