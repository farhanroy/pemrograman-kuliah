#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 10
 
typedef struct {
    int no;
    char nama[50];
    int nilai;
} Data;

void choose();
void input(Data []);
void show(Data [], int);
void copy(Data [], Data []);
void insertionSort(Data [], int);
void sequentialSearch(Data [], char [], int);
int binarySearch(Data [], int, int, char [], int);

Data data[MAX];
 
int n;

int main() {
    input(data);
    choose();

    return 0;
}

void choose() {
    Data temp[n];
    char key[50];
    int menu, showMode, searchMode;

    copy(data, temp);

    printf("MENU METODE SEARCHING\n");
    printf("1. Tampilkan data\n2. Sequential Search\n3. Binary Search\n4. Keluar\n");
    printf("Pilih menu[1/2/3]: ");
    scanf("%d", &menu);

    if (menu == 1) {
        printf("BENTUK DATA: \n");
        printf("1. Tidak terurut\n2. Terurut Berdasarkan No\n");
        printf("Masukan pilihan [1/2]: ");
        scanf("%d", &showMode);

        show(temp, showMode);
    } else if (menu == 2 || menu == 3) {
        printf("PENCARIAN BERDASARKAN: \n");
        printf("1. No\n2. Nama\n");
        printf("Masukan pilihan [1/2]: ");
        scanf("%d", &searchMode);
        fflush(stdin);
        printf("Data yang ingin dicari: ");
        scanf("%[^\n]%*c", key);

        if (menu == 2) {
            sequentialSearch(temp, key, searchMode);
        } else {
            insertionSort(temp, searchMode);
            int result = binarySearch(temp, 0, n-1, key, searchMode);
            if (result != -1) {
                printf ("data ketemu di index ke-%d\n", result);
            } else {
                printf ("data tidak ketemu\n");
            }
        }

    } else {
        exit(0);
    }

    choose();
    
}

void sequentialSearch(Data d[], char key[], int searchMode) {
    int i, found = 0, kondisi, kondisi2;

    for (i = 0; i < n; i++) {
        if (searchMode == 1) {
            kondisi = d[i].no == strtol(key, NULL, 10);
            //kondisi2 = d[i].no > strtol(key, NULL, 10);
        } else {
            kondisi = strcmp(d[i].nama, key) == 0;
            //kondisi2 = 0;
        }
        if (kondisi) {
            found = 1;
            break;
        }
    }

    if (found) {
        printf("Ketemu di index ke-%d\n", i);
    } else {
        printf("Tidak ketemu\n");
    }
}

int binarySearch(Data d[], int left, int right, char key[], int searchMode) {
    int kondisi, kondisi2;

    if (right >= left) {
        int middle = left + (right - left) / 2;

        if (searchMode == 1) {
            kondisi = d[middle].no == strtol(key, NULL, 10);
            kondisi2 = d[middle].no > strtol(key, NULL, 10);
        } else {
            kondisi = strcmp(d[middle].nama, key) == 0;
            kondisi2 = 0;
        }

        if (kondisi)
            return middle;

        if (kondisi2)
            return binarySearch(d, left, middle - 1, key, searchMode);

        return binarySearch(d, middle + 1, right, key, searchMode);
    }
    return -1;
}

void input(Data data[]) {
    printf("Berapa jumlah siswa: ");
    scanf("%d", &n);
 
    for (int i = 0; i < n; i++) {
        printf("Masukan no: ");
        scanf("%d", &data[i].no);
        fflush(stdin);
        printf("Masukan nama: ");
        scanf("%[^\n]%*c", data[i].nama);
        fflush(stdin);
        printf("Masukan nilai: ");
        scanf("%d", &data[i].nilai);
        fflush(stdin);
    }
}

void show(Data d[], int showMode) {
    if (showMode == 2) {
        insertionSort(d, showMode);
    }

    for (int i = 0; i < n; i++) {
         printf("%d %s %d\n", d[i].no, d[i].nama, d[i].nilai);
    }
}
 
void copy(Data arr[], Data copy[]) {
  for (int i = 0; i < n; i++) {
    copy[i].no = arr[i].no;
    copy[i].nilai = arr[i].nilai;
    strcpy(copy[i].nama, arr[i].nama);
  }
}

void insertionSort(Data arr[], int searchMode) {
    int i, j, kondisi;
    Data key;
    for(i = 1; i < n; i++) {
        key = arr[i];
        j = i-1;
        if (searchMode == 1) {
            kondisi = arr[j].no > key.no;
        } else {
            kondisi =strcmp(arr[i].nama, key.nama) > 0;
        }
        while(j >= 0 && kondisi) {
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = key;
    }
}