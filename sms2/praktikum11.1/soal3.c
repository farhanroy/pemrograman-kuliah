#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
 
#define MAX 20000
 
void choose();
void generate(int []);
void insertionSort(int [], int);
void selectionSort(int [], int);
void bubbleSort(int [], int);
void shellSort(int [], int);
void show(int []);
void swap(int *, int *);
void reset();
void copy(int [], int []);

int arr[MAX];
 
int main() {
    generate(arr);
    choose();
    return 0;
}
 
void choose() {
    int menu, mode;

    int temp[MAX];
    
    copy(arr, temp);
 
    printf("Pilih menu\n");
    printf("1. Insertion Sort\n2. Selection Sort\n3. Bubble Sort\n4. Shell Sort\n5. Exit\n");
 
    printf("Masukan menu: ");
    scanf("%d", &menu);
 
    if (menu < 5) {
        printf("Pilih mode\n");
        printf("1. Ascending\n2. Descending\n");
        printf("Masukan mode: ");
        scanf("%d", &mode);
    }
 
    clock_t begin = clock();
    switch(menu) {
        
        case 1:
            insertionSort(temp, mode);
            break;
        case 2:
            selectionSort(temp, mode);
            break;
        case 3:
            bubbleSort(temp, mode);
            break;
        case 4:
            shellSort(temp, mode);
            break;
        default:
            exit(0);
    
    }
 
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("%f\n", time_spent);
 
    
    choose();
}
 
void generate(int arr[]){
  for(int i = 0; i < MAX; i++) {
    arr[i] = rand()/1000;
  }
}
 
void swap(int* xp, int* yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}
 
 
void insertionSort(int arr[], int mode) {
    int i, j, key, kondisi;
    for(i = 1; i < MAX; i++) {
        key = arr[i];
        j = i-1;
        if (mode == 1) {
            kondisi = arr[j] < key;
        } else {
            kondisi = arr[j] > key;
        }

        while(j >= 0 && kondisi) {
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = key;
    }
}
 
void selectionSort(int arr[], int mode) {
    int i, j, min, kondisi;
    for(i = 0; i < MAX - 1; i++) {
        min = i;
        for(j = i+1; j < MAX; j++)  {
            if (mode == 1) {
                kondisi = arr[j] < arr[min];
            } else {
                kondisi = arr[j] > arr[min];
            }

            if (kondisi) {
                min = j;
            }
            
        }
        swap(&arr[min], &arr[i]);
    }
}
 
 
void bubbleSort(int arr[], int mode)
{
    int i, j, flag = 1, r = 0, n = MAX, kondisi;
    for (i = 0; i < n - 1 && flag ; i++) {
        flag = 0;
            for (j = 0; j < n - i - 1; j++) {
                if (mode == 1) 
                  kondisi = arr[j] > arr[j + 1];
                else
                  kondisi = arr[j] < arr[j + 1];
 
                if (kondisi) {
                    swap(&arr[j], &arr[j + 1]);
                    flag = 1;
                }
            }
    }
                
}
 
void shellSort(int arr[], int mode) {
    int n = MAX, kondisi;
    for (int gap = n/2; gap > 0; gap /= 2)
    {
        for (int i = gap; i < n; i += 1)
        {
            int temp = arr[i];
            int j;         
 
            if (mode == 1) 
                  kondisi = arr[j - gap] > temp;
                else
                  kondisi = arr[j - gap] < temp;
 
            for (j = i; j >= gap && kondisi; j -= gap)
                arr[j] = arr[j - gap];
            
            arr[j] = temp;
        }
    }
}
 
void show(int array[]) {
  for (int i = 0; i < MAX; ++i) {
    printf("%d  ", array[i]);
  }
  printf("\n");
}

void copy(int arr[], int copy[]) {
  for (int i = 0; i < MAX; i++) {
    copy[i]= arr[i];
  }
}
 
void reset() {
  srand(time(NULL));
}