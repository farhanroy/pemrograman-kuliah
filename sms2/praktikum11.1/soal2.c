#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define MAX 20000

void choose();
void generate(int []);
void bubbleSort(int [], int);
void shellSort(int [], int);
void show(int []);
void swap(int *, int *);
void reset();

int c, m, s;

int main() {
    choose();

    return 0;
}

void choose() {
    int menu, mode;
    int arr[MAX];

    reset();

    generate(arr);

    printf("Pilih menu\n");
    printf("1. Bubble Sort\n2. Shell Sort\n3. Exit\n");

    printf("Masukan menu: ");
    scanf("%d", &menu);

    if (menu != 3) {
        printf("Pilih mode\n");
        printf("1. Ascending\n2. Descending\n");
        printf("Masukan mode: ");
        scanf("%d", &mode);
    }

    clock_t begin = clock();
    switch(menu) {
        
        case 1:
            bubbleSort(arr, mode);
            break;
        case 2:
            shellSort(arr, mode);
            break;
        default:
            exit(0);
    
    }

    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("%f\n", time_spent);

    printf("Comparison: %d \nMovement: %d \nSwap: %d\n", c, m, s);

    
    choose();
}

void generate(int arr[]){
  for(int i = 0; i < MAX; i++) {
    arr[i] = rand()/1000;
  }
}

void swap(int* xp, int* yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}


void bubbleSort(int arr[], int mode)
{
    int i, j, flag = 1, r = 0, n = MAX, kondisi;
    for (i = 0; i < n - 1 && flag ; i++) {
        flag = 0;
            for (j = 0; j < n - i - 1; j++) {
                if (mode == 1) 
                  kondisi = arr[j] > arr[j + 1];
                else
                  kondisi = arr[j] < arr[j + 1];

                if (kondisi) {
                    swap(&arr[j], &arr[j + 1]);
                    s++;
                    m += 3;
                    flag = 1;
                }
                c++;
            }
    }
                
}

void shellSort(int arr[], int mode) {
    int n = MAX, kondisi;
    for (int gap = n/2; gap > 0; gap /= 2)
    {
        for (int i = gap; i < n; i += 1)
        {
            int temp = arr[i];
            int j;         

            if (mode == 1) 
                  kondisi = arr[j - gap] > temp;
                else
                  kondisi = arr[j - gap] < temp;

            for (j = i; j >= gap && kondisi; j -= gap)
                arr[j] = arr[j - gap];
                s++;
                c++;
                m += 3;
            
            arr[j] = temp;
        }
    }
}

void show(int array[]) {
  for (int i = 0; i < MAX; ++i) {
    printf("%d  ", array[i]);
  }
  printf("\n");
}

void reset() {
  c = 0;
  m = 0;
  s = 0;

  srand(time(NULL));
}