#include <stdio.h>

void swap(int *, int *);
void bubbleSort(int [], int);
void printArray(int [], int);

int c, m, s;

int main() {
    int arr[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
    int n = sizeof(arr) / sizeof(arr[0]);
    bubbleSort(arr, n);
    printf("Sorted array: \n");
    printArray(arr, n);

    printf("Comparison: %d \nMovement: %d \nSwap: %d", c, m, s);
    return 0;
}

void swap(int* xp, int* yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void bubbleSort(int arr[], int n)
{
    int i, j, flag = 1, r = 0;
    for (i = 0; i < n - 1 && flag ; i++) {
        flag = 0;
            for (j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    swap(&arr[j], &arr[j + 1]);
                    s++;
                    m += 3;
                    flag = 1;
                }
                c++;
            }
    }
                
}

void printArray(int arr[], int size)
{
    int i;
    for (i = 0; i < size; i++)
        printf("%d ", arr[i]);
    printf("\n");
}