#include <stdio.h>

void swap(int *, int *);
void shellSort(int [], int);
void printArray(int [], int);

int c, m, s;

int main() {
    int arr[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
    int n = sizeof(arr) / sizeof(arr[0]);
    shellSort(arr, n);
    printf("Sorted array: \n");
    printArray(arr, n);

    printf("Comparison: %d \nMovement: %d \nSwap: %d", c, m, s);
    return 0;
}

void swap(int* xp, int* yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void shellSort(int arr[], int n) {
    int jarak = n, flag, i;
    while (jarak > 1) {
        jarak /= jarak;
        flag = 1;
        while(flag) {
            flag = 0;
            for (i = 0; i < (n - jarak); i++) {
                if (arr[i] > arr[i+jarak]) {
                    swap(&arr[i], &arr[i+jarak]);
                    flag = 1;
                    s++;
                    m += 3;
                }
                c++;
            }
        }
    }
}

void printArray(int arr[], int size)
{
    int i;
    for (i = 0; i < size; i++)
        printf("%d ", arr[i]);
    printf("\n");
}