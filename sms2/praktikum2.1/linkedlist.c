#include <stdio.h>
#include <stdlib.h>

typedef struct simpul Node;
struct simpul {
    int data;
    Node *next;
};

Node *head = NULL;
Node *p;

// Choose menu
void chooseMenu();
// Allocate memory to pointer p
void allocateMemory();
// Insert first data in linked list
void insertFirst();
// Insert in the middle of linked list
void insertLast();
// After some key, insert value
void insertAfter();
// Before some key, insert value
void insertBefore();
// Showing all of node
void getAllNode();
// Going out
void outProgram();

int main() {
    chooseMenu();
    return 0;
}

void chooseMenu() {
    int menu;
    printf("Menu Insert\n");
    printf("1. Awal\n2. Akhir\n3. After\n4. Before\n5. Exit\n");
    printf("Pilih menu: ");
    scanf("%d", &menu);
    switch(menu) {
        case 1:
            insertFirst();
            break;
        case 2:
            insertLast();
            break;
        case 3:
            insertAfter();
            break;
        case 4:
            insertBefore();
            break;
        case 5:
            outProgram();
            break;
        default:
            outProgram();
            break;
    }
}

void allocateMemory() {
    p = (Node *) malloc(sizeof(Node));
    
    if (p == NULL) {
        printf("Cannot allocate memory");
        exit(0);
    } else {
        p->data = 
    }
}

void getAllNode() {
    Node *temp = head;
    while (temp != NULL) {
        printf("%d ", temp->data);
        temp = temp->next;
    }
}

void insertFirst() {
    int value;
    char answer = 'y';

    printf("Single linked list - Insert First\n");

    while(answer == 'y' || answer == 'Y') {
        printf("Input a value: ");
        scanf("%d", &value);
        
        allocateMemory();
        
        p->data = value;
        p->next = NULL;
        
        if (head == NULL) {
            head = p;
        } else {
            p->next = head;
        }

        head = p;

        printf("Enter again (Y/N) ? ");
        fflush(stdin);
        scanf("%c", &answer);
    }
    
    getAllNode();
    outProgram();
}

void insertLast() {
    Node *tail;
    int value;
    char answer = 'y';

    printf("Single linked list - Insert Last\n");

    while(answer == 'y' || answer == 'Y') {
        printf("Input a value: ");
        scanf("%d", &value);
        
        allocateMemory();
        
        p->data = value;
        p->next = NULL;
        
        if (head == NULL) {
            head = p;
        } else {
            tail = head;
            while(tail->next != NULL) {
                tail = tail->next;
            }
            tail->next = p;
        }

        printf("Enter again (Y/N) ? ");
        fflush(stdin);
        scanf("%c", &answer);
    }

    getAllNode();
    outProgram();
}

void insertAfter() {
    Node *after;
    int value, key;
    char answer = 'y';

    printf("Single linked list - Insert After\n");

    while(answer == 'y' || answer == 'Y') {

        printf("Enter key: ");
        scanf("%d", &key);

        printf("Input a value: ");
        scanf("%d", &value);
        
        allocateMemory();
        
        p->data = value;
        p->next = NULL;
        
        if (head == NULL) {
            head = p;
        } else {
            after = head;
            while(after->data != key) {
                if (after->next == NULL) {
                    printf("Key not found\n");
                    exit(0);
                } else {
                 after = after->next;   
                }
            }
            p->next = after->next;
                    after->next = p;
        }

        printf("Enter again (Y/N) ? ");
        fflush(stdin);
        scanf("%c", &answer);
    }

    getAllNode();
    outProgram();
}
void insertBefore() {
    Node *temp, *previous;
    int value, key;
    char answer = 'y';

    printf("Single linked list - Insert Before\n");

    while(answer == 'y' || answer == 'Y') {

        printf("Input a value: ");
        scanf("%d", &value);

        printf("Enter key: ");
        scanf("%d", &key);
        
        allocateMemory();
        
        p->data = value;
        p->next = NULL;
        
        if (head == NULL) {
            head = p;
        } else if (head->data == key) {
            
            p->next = head;
            head = p;

        } else {
            
            temp = head;
            
            do {
                if (temp->next == NULL) {
                    printf("Key Not Found");
                    outProgram();
                } else {      
                    temp = temp->next;
                }
            } while(temp->data != key);
            

            p->next = temp;
            previous->next = p;
        }

        printf("Enter again (Y/N) ? ");
        fflush(stdin);
        scanf("%c", &answer);
    }

    getAllNode();
    outProgram();
}

void outProgram() {
    char answer;

    printf("\nDo you want to go out ? ");
    fflush(stdin);
    scanf("%c", &answer);

    if (answer == 'y' || answer == 'Y') {
        printf("\nGoing out");
        exit(0);
    } else {
        chooseMenu();
    }
    
}