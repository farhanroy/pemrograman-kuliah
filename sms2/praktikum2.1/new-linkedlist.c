#include <stdio.h>
#include <stdlib.h>

typedef struct node Node;

struct node {
    int data;
    Node *next;
};

Node *head;
Node *newNode;

void initial();

void chooseMenu();

void createNewNode();

void getAllNode();

void processMenu(int);

void insertFirst();

void insertLast();

void insertAfter();

void insertBefore();

int main() {
    initial();
    chooseMenu();
    return 0;
}

void initial() {
    head = NULL;
}

void chooseMenu() {
    int menu;
    printf("Menu Insert\n");
    printf("1. Awal\n2. Akhir\n3. After\n4. Before\n5. Exit\n");
    printf("Pilih menu: ");
    scanf("%d", &menu);
    processMenu(menu);
}

void processMenu(int menu) {
    char answer = 'y';

    while(answer == 'y' || answer == 'Y') {
        if (menu == 1) {
            insertFirst();
        } else if (menu == 2) {
            insertLast();
        } else if (menu == 3) {
            insertAfter();
        } else if (menu == 4) {
            insertBefore();
        } else {
            exit(0);
        }

        printf("Are you input again (y/n) ? ");
        fflush(stdin);
        scanf("%c", &answer);
    }
    
    getAllNode();
    chooseMenu();
}

void createNewNode() {
    newNode = (Node *) malloc(sizeof(Node));
    
    if (newNode == NULL) {
        printf("Cannot allocate memory");
        exit(0);
    } else {
        printf("Enter value for new node: ");
        scanf("%d", &newNode->data);
        newNode->next = NULL;
    }
}

void getAllNode() {
    Node *temp = head;
    while (temp != NULL) {
        printf("%d \n", temp->data);
        temp = temp->next;
    }
}

void insertFirst() {
    createNewNode();

    if (head != NULL) {
        newNode->next = head;
    } 

    head = newNode;
}

void insertLast() {
    Node *current;

    createNewNode();

    if (head == NULL) {
        head = newNode;
    } else {
        current = head;
        while(current->next != NULL) {
            current = current->next;
        }
        // add newNode in current->next
        current->next = newNode;
        // change current state to last node
        current = current->next;
    }
}

void insertAfter() {
    int key;
    Node *current;

    createNewNode();

    if (head == NULL) {
        head = newNode;
    } else {

        printf("Append value after: ");
        scanf("%d", &key);

        current = head;
        while(current->data != key) {
            if (current->next == NULL) {
                printf("Key not found");
                exit(0);
            } else {
                current = current->next;
            }
        }

        newNode->next = current->next;
        current->next = newNode;
    }
}

void insertBefore() {
    int key;
    Node *current;
    Node *previous;

    createNewNode();

    if (head == NULL) {
        head = newNode;
    } else {
        printf("Append value before: ");
        scanf("%d", &key);

        if (head->data == key) {
            newNode->next = head;
            head = newNode;
        } else {
            current = head;
            while(current->data != key) {
                if (current->next == NULL) {
                    printf("Key not found");
                    exit(0);
                } else {
                    previous = current;
                    current = current->next;
                }
            }

            previous->next = newNode;
            newNode->next = current;
        }
    }
}


