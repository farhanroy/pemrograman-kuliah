#include<stdio.h>
#include<stdlib.h>

#define max 20

typedef struct node Node;

struct node {
    int no;
    char nama[max];
    float nilai;
    Node *next;
    Node *previous;
};

Node *head;
Node *newNode;

void chooseMenu();
void processMenu(int);

void exitProgram();

void createNewNode();
void deleteNode(Node *);
void getAllItems();
void insertItem();
void deleteItem();
void searchItem();
void getRataRata();

void deleteItemAtFirst(Node *);

int main() {
    head = NULL;
    chooseMenu();
    return 0;
}

void chooseMenu() {
    int menu;
    printf("Menu Insert\n");
    printf("1. Penambahan secara terurut\n");
    printf("2. Pencarian data\n");
    printf("3. Penghapusan data\n");
    printf("4. Rata - rata\n");
    printf("5. Keluar\n");
    printf("Pilih menu: ");
    scanf("%d", &menu);
    processMenu(menu);
}

void processMenu(int menu) {

    switch(menu) {
        case 1:
            insertItem();
            break;
        case 2:
            searchItem();
            break;
        case 3: 
            deleteItem();
            break;
        case 4: 
            getRataRata();
            break;
        default:
            exitProgram();
    }
}

void exitProgram() {
    printf("\nExit from program");
    exit(0);
}

void createNewNode() {
    newNode = (Node *) malloc(sizeof(Node));
    
    if (newNode == NULL) {
        printf("Cannot allocate memory");
        exit(0);
    } else {
        printf("No: ");
        fflush(stdin);
        scanf("%d", &newNode->no);

        printf("Name: ");
        fflush(stdin);
        scanf("%[^\n]s",newNode->nama);

        printf("Value: ");
        fflush(stdin);
        scanf("%f", &newNode->nilai);

        newNode->next = NULL;
        newNode->previous = NULL;
    }
}

void getAllItems() {
    Node *temp = head;
    printf("\nNo\tName\tValue\n");
    while (temp != NULL) {
        printf("%d\t%s\t%.1f\n", temp->no, temp->nama, temp->nilai);
        temp = temp->next;
    }
}

void getRataRata() {
    Node *current = head;
    float total = 0;
    int counter = 0;
    while (current != NULL) {
        total += current->nilai;
        counter++;
        current = current->next;
    }
    
    float rata = total/counter;
        printf("Rata - rata = %.1f\n", rata);
    chooseMenu();
}

void insertItem() {
    Node *current;
    createNewNode();
    if (head == NULL) {
        head = newNode;
    } else if(head->no >= newNode->no) {
        newNode->next = head;
        head = newNode; 
    } else {
        current = head;
        while(current->next != NULL && current->next->no < newNode->no) {
            current = current->next;
        }

        newNode->next = current->next;
        if (current->next != NULL) 
            newNode->next->previous = newNode;
        newNode->previous = current;
        current->next = newNode;

    }
    getAllItems();
    chooseMenu();
}

void searchItem() {
    int key, counter = 0;
    Node *temp;
    
    temp = head;

    printf("\nSearch key: ");
    fflush(stdin);
    scanf("%d", &key);

    while (temp != NULL) {

        if (temp->no == key) {
            counter++;
        }
        temp = temp->next;
    }

    if (counter > 0) {
        printf("\n data %d ada %d kali\n", key, counter);
    } else {
        printf("\ndata tidak ditemukan\n");
    }
    chooseMenu();
}

void deleteItem() {
    int key, counter = 0;

    Node *current = head;

    printf("\nSearch key: ");
    scanf("%d", &key);

    if (head == NULL) {
            chooseMenu();
    } else if (head->no == key) {
            deleteItemAtFirst(current);
    } else {
            while (current->no != key) {
                if (current->next == NULL) {
                    exitProgram();
                }
                current = current->next;
            }
            if (current->next != NULL) {
                current->next->previous = current->previous;

            current->previous->next = current->next;
        }
    }
    
    deleteNode(current);

    getAllItems();
    chooseMenu();
}

void deleteItemAtFirst(Node *current) {
    if(head->next == NULL) {
        head = NULL;
    } else {
        head = current->next;
        current->previous = NULL;
    }
}

void deleteNode(Node *delete) {
    free(delete);
    delete = NULL;
}