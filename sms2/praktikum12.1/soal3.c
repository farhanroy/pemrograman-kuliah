#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define MAX 1000000

void choose();
void insertionSort(int [], int);
void selectionSort(int [], int);
void bubbleSort(int [], int);
void shellSort(int [], int);
void mergeSort(int [], int, int, int);
void merge(int [], int, int, int, int);
void quickSort(int [], int, int, int);
int partition(int [], int, int, int);
void show(int []);
void swap(int *, int *);
void copy(int [], int []);
void generate(int []);

int arr[MAX];
int n;

int main() {
    printf("Masukan jumlah array: ");
    scanf("%d", &n);

    if (n < 30000) 
        exit(0);

    generate(arr);
    choose();
    return 0;
}

void choose() {
    int menu, mode;

    int temp[n];
    
    copy(arr, temp);
 
    printf("Pilih menu\n");
    printf("1. Insertion Sort\n2. Selection Sort\n3. Bubble Sort\n4. Shell Sort\n5. Merge Sort\n6. Quick Sort\n7. Exit\n");
 
    printf("Masukan menu: ");
    scanf("%d", &menu);
 
    if (menu < 7) {
        printf("Pilih mode\n");
        printf("1. Ascending\n2. Descending\n");
        printf("Masukan mode: ");
        scanf("%d", &mode);
    }
 
    clock_t begin = clock();

    switch(menu) {
        
        case 1:
            insertionSort(temp, mode);
            break;
        case 2:
            selectionSort(temp, mode);
            break;
        case 3:
            bubbleSort(temp, mode);
            break;
        case 4:
            shellSort(temp, mode);
            break;
        case 5:
            mergeSort(temp, 0, n - 1, mode);
            break;
        case 6:
            quickSort(temp, 0, n - 1, mode);
            break;
        default:
            exit(0);
    
    }
    
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Data %d = %f\n",n, time_spent);
 
    choose();
}
 
void swap(int* xp, int* yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}
 
 
void insertionSort(int arr[], int mode) {
    int i, j, key, kondisi;
    for(i = 1; i < n; i++) {
        key = arr[i];
        j = i-1;
        if (mode == 1) {
            kondisi = arr[j] < key;
        } else {
            kondisi = arr[j] > key;
        }

        while(j >= 0 && kondisi) {
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = key;
    }
}
 
void selectionSort(int arr[], int mode) {
    int i, j, min, kondisi;
    for(i = 0; i < n - 1; i++) {
        min = i;
        for(j = i+1; j < n; j++)  {
            if (mode == 1) {
                kondisi = arr[j] < arr[min];
            } else {
                kondisi = arr[j] > arr[min];
            }

            if (kondisi) {
                min = j;
            }
            
        }
        swap(&arr[min], &arr[i]);
    }
}
 
 
void bubbleSort(int arr[], int mode)
{
    int i, j, flag = 1, r = 0, kondisi;
    for (i = 0; i < n - 1 && flag ; i++) {
        flag = 0;
            for (j = 0; j < n - i - 1; j++) {
                if (mode == 1) 
                  kondisi = arr[j] > arr[j + 1];
                else
                  kondisi = arr[j] < arr[j + 1];
 
                if (kondisi) {
                    swap(&arr[j], &arr[j + 1]);
                    flag = 1;
                }
            }
    }
                
}
 
void shellSort(int arr[], int mode) {
    int kondisi;
    for (int gap = n/2; gap > 0; gap /= 2)
    {
        for (int i = gap; i < n; i += 1)
        {
            int temp = arr[i];
            int j;         
 
            if (mode == 1) 
                  kondisi = arr[j - gap] > temp;
                else
                  kondisi = arr[j - gap] < temp;
 
            for (j = i; j >= gap && kondisi; j -= gap)
                arr[j] = arr[j - gap];
            
            arr[j] = temp;
        }
    }
}

void mergeSort(int arr[], int l, int r, int mode)
{
    if (l < r) {
        int m = l + (r - l) / 2;
  
        mergeSort(arr, l, m, mode);
        mergeSort(arr, m + 1, r, mode);
  
        merge(arr, l, m, r, mode);
    }
}

void merge(int arr[], int l, int m, int r, int mode)
{
    int i, j, k;
    int n1 = m - l + 1;
    int n2 = r - m;
  
    int L[n1], R[n2];
  
    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        R[j] = arr[m + 1 + j];
  
    i = 0; 
    j = 0; 
    k = l; 
    while (i < n1 && j < n2) {
        int kondisi = mode == 1 ? L[i] <= R[j] : L[i] >= R[j];
        if (kondisi) {
            arr[k] = L[i];
            i++;
        }
        else {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    while (i < n1) {
        arr[k] = L[i];
        i++;
        k++;
    }
    
    while (j < n2) {
        arr[k] = R[j];
        j++;
        k++;
    }
}

int partition (int arr[], int low, int high, int mode)
{
    int pivot = arr[high];
    int i = (low - 1); 
 
    for (int j = low; j <= high - 1; j++)
    {
        int kondisi = mode == 1 ? arr[j] < pivot : arr[j] > pivot;
        if (kondisi)
        {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}


void quickSort(int arr[], int low, int high, int mode)
{
    if (low < high)
    {
        int pi = partition(arr, low, high, mode);

        quickSort(arr, low, pi - 1, mode);
        quickSort(arr, pi + 1, high, mode);
    }
}
 
void generate(int arr[]){
  for(int i = 0; i < n; i++) {
    arr[i] = rand()/1000;
  }
} 

void show(int array[]) {
  for (int i = 0; i < n; ++i) {
    printf("%d  ", array[i]);
  }
  printf("\n");
}

void copy(int arr[], int copy[]) {
  for (int i = 0; i < n; i++) {
    copy[i]= arr[i];
  }
}