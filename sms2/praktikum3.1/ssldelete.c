#include<stdio.h>
#include<stdlib.h>

typedef struct node Node;

struct node {
    int data;
    Node *next;
};

Node *head;
Node *newNode;

void chooseMenu();
void chooseMenuInsert();
void chooseMenuDelete();

void processMenuInsert(int);
void processMenuDelete(int);

void createNewNode();
void freeNode(Node *);
void getAllNode();

void insertFirst();
void insertLast();
void insertAfter();
void insertBefore();

void deleteFirst();
void deleteLast();
void deleteCertain();

void programExit();

int main() {
    head = NULL;
    chooseMenu();
}

void chooseMenu() {
    int menu;
    printf("Menu:\n");
    printf("1. Insert\n2. Delete\n3. Exit\n");
    printf("Pilih menu: ");
    scanf("%d", &menu);

    if (menu == 1) 
        chooseMenuInsert();
    else if (menu == 2)
        chooseMenuDelete();
    else 
        programExit();
}

void chooseMenuInsert() {
    int menu;
    printf("Menu:\n");
    printf("1. Awal\n2. Akhir\n3. After\n4. Before\n");
    printf("Pilih menu: ");
    scanf("%d", &menu);
    processMenuInsert(menu);
}

void chooseMenuDelete() {
    int menu;
    printf("Menu:\n");
    printf("1. Awal\n2. Akhir\n3. Tertentu\n");
    printf("Pilih menu: ");
    scanf("%d", &menu);
    processMenuDelete(menu);
}

void processMenuInsert(int menu) {
    char answer = 'y';

    while(answer == 'y' || answer == 'Y') {
        if (menu == 1) {
            insertFirst();
        } else if (menu == 2) {
            insertLast();
        } else if (menu == 3) {
            insertAfter();
        } else if (menu == 4) {
            insertBefore();
        } else {
            programExit();
        }

        printf("Are you input again (y/n) ? ");
        fflush(stdin);
        scanf("%c", &answer);
    }
    
    getAllNode();
    chooseMenu();
}

void processMenuDelete(int menu) {
    if (menu == 1) {
            deleteFirst();
        } else if (menu == 2) {
            deleteLast();
        } else if (menu == 3) {
            deleteCertain();
        } else {
            programExit();
        }
}

void createNewNode() {
    newNode = (Node *) malloc(sizeof(Node));
    
    if (newNode == NULL) {
        printf("Cannot allocate memory");
        exit(0);
    } else {
        printf("Enter value for new node: ");
        scanf("%d", &newNode->data);
        newNode->next = NULL;
    }
}

void freeNode(Node *delete) {
    free(delete);
    delete = NULL;
}

void getAllNode() {
    Node *temp = head;
    while (temp != NULL) {
        printf("%d \n", temp->data);
        temp = temp->next;
    }
}

void insertFirst() {
    createNewNode();

    if (head != NULL) {
        newNode->next = head;
    } 

    head = newNode;
}

void insertLast() {
    Node *current;

    createNewNode();

    if (head == NULL) {
        head = newNode;
    } else {
        current = head;
        while(current->next != NULL) {
            current = current->next;
        }
        // add newNode in current->next
        current->next = newNode;
        // change current state to last node
        current = current->next;
    }
}

void insertAfter() {
    int key;
    Node *current;

    createNewNode();

    if (head == NULL) {
        head = newNode;
    } else {

        printf("Append value after: ");
        scanf("%d", &key);

        current = head;
        while(current->data != key) {
            if (current->next == NULL) {
                printf("Key not found");
                exit(0);
            } else {
                current = current->next;
            }
        }

        newNode->next = current->next;
        current->next = newNode;
    }
}

void insertBefore() {
    int key;
    Node *current;
    Node *previous;

    createNewNode();

    if (head == NULL) {
        head = newNode;
    } else {
        printf("Append value before: ");
        scanf("%d", &key);

        if (head->data == key) {
            newNode->next = head;
            head = newNode;
        } else {
            current = head;
            while(current->data != key) {
                if (current->next == NULL) {
                    printf("Key not found");
                    exit(0);
                } else {
                    previous = current;
                    current = current->next;
                }
            }

            previous->next = newNode;
            newNode->next = current;
        }
    }
}

/*
* Single Linked List Delete
*/

void deleteFirst() {
    Node *current;
    current = head;
    if (head == NULL) {
        programExit();
    } else if(head->next == NULL) {
        head = NULL;
    } else {
        head = current->next;
    }

    freeNode(current);

    getAllNode();
    chooseMenu();
}

void deleteLast() {
    Node *current, *temp;

    current = head;
    if (head == NULL) {
        programExit();
    } else if (head->next == NULL) {
        head = NULL;
    } else {
        while(current->next != NULL) {
            temp = current;
            current = current->next;
        }
        temp->next = NULL;
    }

    freeNode(current);

    getAllNode();
    chooseMenu();
}

void deleteCertain() {
    Node *current, *temp;
    int key;

    printf("Masukan key: ");
    scanf("%d", &key);

    current = head;
    
    if (head == NULL) {
        programExit();
    } else if (head->data == key) {
        deleteFirst();
    } else {
        while(current->data != key) {
            if (current->next == NULL) {
                programExit();
            } else {
                temp = current;
                current = current->next;
            }
        }
        temp->next = current->next;
    }

    freeNode(current);
    getAllNode();
    chooseMenu();
}


void programExit() {
    printf("\nProgram keluar\n");
    exit(0);
}