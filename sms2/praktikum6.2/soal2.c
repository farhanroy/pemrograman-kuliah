#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX 20

typedef char type;

typedef struct {
    char data[MAX];
    int count;
} Stack;

void initialize(Stack *);
int isEmpty(Stack *);
int isFull(Stack *);
void push(Stack *, char data);
char pop(Stack *);
char peek(Stack *);
int isPalindrome(char []);

int main() {
    char palindrome[MAX], answer = 'y';

    printf("Mengecek Palindrom\n");

    while(answer == 'y' || answer == 'Y') {
        printf("Masukan kata yang ingin dicek: ");
        fflush(stdin);
        gets(palindrome);

        printf("\n%d\n", isPalindrome(palindrome));

        printf("Apakah mau input lagi ?");
        scanf("%c", &answer);
    }

    return 0;
}

void initialize(Stack *stack) {
    stack->count = 0;
}

int isEmpty(Stack *stack) {
    return stack->count == 0;
}

int isFull(Stack *stack) {
    return stack->count >= MAX;
}

char pop(Stack *stack) {
    if (!isEmpty(stack)) {
        return stack->data[--stack->count];
    }
    return ' ';
}

void push(Stack *stack, char d) {
    if(!isFull(stack)) {
        stack->data[stack->count++] = d;
    }
}

int isPalindrome(char palindrome[]) {
    Stack stack;
    initialize(&stack);

    int i = 0;
    while(i < strlen(palindrome)) {
        push(&stack, palindrome[i]);
        i++;
    }

    i = 0;
    while(!isEmpty(&stack)) {
        if (pop(&stack) != palindrome[i])
            return 0;
        i++;
    }

    return 1;

}
