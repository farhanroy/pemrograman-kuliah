#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX 20

typedef char type;

typedef struct {
    char data[MAX];
    int count;
} Stack;


void chooseMenu();

void initialize(Stack *);
int isEmpty(Stack *);
int isFull(Stack *);
void push(Stack *, char data);
int pop(Stack *);
void getAllData(Stack *);

void toBiner(Stack *);
void toOktal(Stack *);
void toHexadecimal(Stack *);

int main() {
    chooseMenu();
    return 0;
}

void chooseMenu() {
    int menu;
    Stack stack;

    initialize(&stack);

    printf("Menu:\n1. Biner\n2. Oktal\n3. Hexadecimal\n4. Exit\n");
    printf("Pilihan: ");
    scanf("%d", &menu);

    switch(menu) {
        case 1:
            toBiner(&stack);
            break;
        case 2:
            toOktal(&stack);
            break;
        case 3:
            toHexadecimal(&stack);
            break;
        default:
            exit(0);
    }
}

void initialize(Stack *stack) {
    stack->count = 0;
}

int isEmpty(Stack *stack) {
    return stack->count == 0;
}

int isFull(Stack *stack) {
    return stack->count >= MAX;
}

int pop(Stack *stack) {
    if (!isEmpty(stack)) {
        return stack->data[--stack->count];
    }
}

void push(Stack *stack, char d) {
    if(!isFull(stack)) {
        stack->data[stack->count++] = d;
    }
}

void getAllData(Stack *stack) {
    while(!isEmpty(stack)) {
        printf("%c", pop(stack));
    }
    puts("");
}


void toBiner(Stack *stack) {
    int number;
    printf("Masukan bilangan desimal: ");
    scanf("%d", &number);

    while (number>0){
        char r = (number % 2) + 48;
        push(stack, r);
        number /= 2;
    }
     getAllData(stack);
}

void toOktal(Stack *stack) {
    int number;
    printf("Masukan bilangan oktal: ");
    scanf("%d", &number);

    while (number>0){
        char r = (number % 8) + 48;
        push(stack, r);
        number /= 8;
    }
    getAllData(stack);
}

void toHexadecimal(Stack *stack) {
    int number;
    printf("Masukan bilangan hexadecimal: ");
    scanf("%d", &number);

    while (number>0){
        int r = number % 16;

        if (r < 10) {
            push(stack, r+48);
        } else {
            push(stack, r+55);
        }

        number /= 16;
    }
    getAllData(stack);
}
