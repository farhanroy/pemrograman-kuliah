#include <stdio.h>
#include <stdlib.h>

typedef struct node Node;

struct node {
    int data;
    Node *next;
};

Node *head;
Node *newNode;

void initial();

void chooseMenu();

void createNewNode();

void getAllNode();

void processMenu(int);

void push();

void pop();


void programExit();

void freeNode(Node *);

int main() {
    initial();
    chooseMenu();
    return 0;
}

void initial() {
    head = NULL;
}

void chooseMenu() {
    int menu;
    printf("Menu Insert\n");
    printf("1. Push\n2. Pop\n3. Show all\n4. Exit\n");
    printf("Pilih menu: ");
    scanf("%d", &menu);
    processMenu(menu);
}

void processMenu(int menu) {
    char answer = 'y';

    while(answer == 'y' || answer == 'Y') {
        if (menu == 1) {
            push();
        } else if (menu == 2) {
            pop();
        } else if (menu == 3) {
            getAllNode();
        } else {
            exit(0);
        }

        printf("Are you input again (y/n) ? ");
        fflush(stdin);
        scanf("%c", &answer);
    }
    
    getAllNode();
    chooseMenu();
}

void createNewNode() {
    newNode = (Node *) malloc(sizeof(Node));
    
    if (newNode == NULL) {
        printf("Cannot allocate memory");
        exit(0);
    } else {
        printf("Enter value for new node: ");
        scanf("%d", &newNode->data);
        newNode->next = NULL;
    }
}

void getAllNode() {
    Node *temp = head;
    while (temp != NULL) {
        printf("%d \n", temp->data);
        temp = temp->next;
    }
}

void push() {
    createNewNode();

    if (head != NULL) {
        newNode->next = head;
    } 

    head = newNode;
}


void pop() {
    Node *current;
    current = head;
    if (head == NULL) {
        programExit();
    } else if(head->next == NULL) {
        head = NULL;
    } else {
        head = current->next;
    }

    freeNode(current);

    getAllNode();
    chooseMenu();
}

void freeNode(Node *n) {
    free(n);
    n = NULL;
}

void programExit() {
    printf("\nExit Program");
    exit(0);
}