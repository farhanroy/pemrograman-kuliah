#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define MAX 100000

void choose();
void generate(int []);
void copy(int [], int []);
void insertionSort(int []);
void sequentialSearchSorted(int [], int);
void sequentialSearchUnsorted(int [], int);
int binarySearch(int [], int, int, int);

int n, arr[MAX];

int main() {
    printf("Masukan jumlah array: ");
    scanf("%d", &n);

    if (n < 25000) {
        exit(0);
    }

    generate(arr);
    choose();

    return 0;
}

void choose() {
    int temp[n], menu, key, result;

    copy(arr, temp);

    printf("Pilih menu\n");
    printf("1. Sequential Unsorted\n2. Sequential Sorted\n3. Binary Sorted\n4. Exit\n");

    printf("Masukan menu: ");
    scanf("%d", &menu);

    srand(time(0));
    key = rand() % 1000 + 1;
    printf("cari data %d\n", key);

    if (menu != 1) {
        insertionSort(temp);
    }

    clock_t begin = clock();

    switch(menu) {    
        case 1:
            sequentialSearchUnsorted(temp, key);
            break;
        case 2:
            sequentialSearchSorted(temp, key);
            break;
        case 3:
            result = binarySearch(temp, 0, n-1, key);
            if (result != -1) {
                printf ("data ketemu di index ke-%d\n", result);
            } else {
                printf ("data tidak ketemu\n");
            }
            break;
        default:
            exit(0);
    }

    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("%f\n", time_spent);

    choose();
}

void sequentialSearchUnsorted(int arr[], int key) {
    int i, found = 0;
    for (i = 0; i < n; i++) {
        if (arr[i] == key) {
            found = 1;
            break;
        }
    }

    if (found) {
        printf("Ketemu di index ke-%d\n", i);
    } else {
        printf("Tidak Ketemu\n");
    }
}

void sequentialSearchSorted(int arr[], int key) {
    int i, found = 0;
    for (i = 0; i < n; i++) {
        if (arr[i] == key) {
            found = 1;
            break;
        }

        if (arr[i] > key) {
            break;
        }
    }

    if (found) {
        printf("Ketemu di index ke-%d\n", i);
    } else {
        printf("Tidak ketemu\n");
    }
}

int binarySearch(int arr[], int left, int right, int key) {
    if (right >= left) {
        int middle = left + (right - left) / 2;

        if (arr[middle] == key)
            return middle;

        if (arr[middle] > key)
            return binarySearch(arr, left, middle - 1, key);

        return binarySearch(arr, middle + 1, right, key);
    }
    return -1;
}

void generate(int arr[]){
  for(int i = 0; i < n; i++) {
    arr[i] = rand() % 1000 + 1;
  }
}


void copy(int arr[], int copy[]) {
  for (int i = 0; i < n; ++i) {
    copy[i] = arr[i];
  }
}

void insertionSort(int arr[]) {
    int i, j, key;
    for(i = 1; i < n; i++) {
        key = arr[i];
        j = i-1;
        while(j >= 0 && arr[j] > key) {
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = key;
    }
}