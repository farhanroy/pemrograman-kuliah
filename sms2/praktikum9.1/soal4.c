#include<stdio.h>
#include<stdlib.h>
#include <time.h>

void chooseMenu();
void iteration(int);
int regularRecursive(int);
int tailRecursive(int, int);

int main() {
  chooseMenu();
  return 0;
}

void chooseMenu() {
  clock_t t;
  double time_taken;

  int menu, n;

  printf("1. Iterasi\n2. Recursive\n3. Recursive Tail\n4. Exit\n");

  printf("Pilih menu: ");
  scanf("%d", & menu);

  printf("Masukan angka: ");
  scanf("%d", & n);

  switch (menu) {
  case 1:
    t = clock();
    iteration(n);
    t = clock() - t;
    time_taken = ((double)t)/CLOCKS_PER_SEC; 
    printf("\ntook %f seconds to execute \n", time_taken);
    break;
  case 2:
    t = clock();
    printf("%d\n", regularRecursive(n));
    t = clock() - t;
    time_taken = ((double)t)/CLOCKS_PER_SEC; 
    printf("\ntook %f seconds to execute \n", time_taken);
    break;
  case 3:
    t = clock();
    printf("%d\n", tailRecursive(n, 1));
    t = clock() - t;
    time_taken = ((double)t)/CLOCKS_PER_SEC; 
    printf("\ntook %f seconds to execute \n", time_taken);
    break;
  default:
    exit(0);
    break;
  }
}

void iteration(int n) {
  int hasil = 1;
  for (int i = 1; i <= n; i++) {
    hasil *= i;
  }
  printf("%d", hasil);
}

int regularRecursive(int n) {
  if (n == 1) {
    return 1;
  } else {
    return n * regularRecursive(n - 1);
  }
}

int tailRecursive(int n, int acc) {
  if (n == 1) {
    return acc;
  } else {
    return tailRecursive(n - 1, n * acc);
  }
}