#include<stdio.h>
#include<stdlib.h>
#include<time.h>
 
void show(int);
 
int main() {

    int n;
    printf("Masukan n: ");
    scanf("%d", &n);
    if (n < 1)
        exit(0);

    clock_t t;
    t = clock();
    show(n);
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; 
    printf("\ntook %f seconds to execute \n", time_taken);
    return 0;
}
 
void show(int n) {
    if (n >= 0) {
        printf("%d ", n);
        show(n-1);
    }
}