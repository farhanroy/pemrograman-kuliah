#include<stdio.h>
#include<stdlib.h>
#include<time.h>
 
void show(int, int);
 
int main() {
    clock_t t;

    int n;
    printf("Masukan n: ");
    scanf("%d", &n);

    if (n < 1)
        exit(0);

    t = clock();
    show(n, 0);
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; 
    printf("\ntook %f seconds to execute \n", time_taken);

    return 0;
}
 
void show(int n, int count) {
    if (count <= n) {
        printf("%d ", count);
        show(n, count+1);
    }
}