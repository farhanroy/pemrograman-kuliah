#include <stdio.h>
#include <stdlib.h>
#include <time.h>
 
void chooseMenu(); 
int factorial(int);
int combination(int, int);
int permutation(int, int);
 
int main()
{
    chooseMenu();
    return 0;
}
 
void chooseMenu()
{
    clock_t t;
    int menu, n, r;
 
    printf("1. Kombinasi\n2. Permutasi\n3. Exit\n");
 
    printf("Pilih menu: ");
    scanf("%d", &menu);
 
    printf("Masukan n: ");
    scanf("%d", &n);
 
    printf("Masukan r: ");
    scanf("%d", &r);
 
    switch (menu)
    {
    case 1:
        t = clock();
        printf("Hasil permutasi = %d", permutation(n, r));
        t = clock() - t;
        time_taken = ((double)t)/CLOCKS_PER_SEC; 
        printf("\ntook %f seconds to execute \n", time_taken);
        break;
    case 2:
        t = clock();
        printf("Hasil kombinasi = %d", combination(n, r));
        t = clock() - t;
        time_taken = ((double)t)/CLOCKS_PER_SEC; 
        printf("\ntook %f seconds to execute \n", time_taken);
        break;
    default:
        exit(0);
        break;
    }
}

int factorial(int n) {
    if (n == 1) {
        return 1;
    } else {
        return n * factorial(n - 1);
    }
}
 
int permutation(int n, int r) {
    int temp;
    temp = factorial(n) / factorial(n-r);
    return temp;
}
 
int combination(int n, int r) {
    int temp;
    temp = factorial(n) / factorial(r) * factorial(n-r);
    return temp;
}