#include<stdio.h>
#include <time.h>
 
#define MAX 20
 
void show(char *);
 
int main() {
    clock_t t;
    
    char text[MAX];
 
    printf("Masukan teks: ");
    scanf("%s", &text);

    t = clock();
    show(text);
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; 
    printf("\ntook %f seconds to execute \n", time_taken);
 
    return 0;
}
 
void show(char *text) {
    if (*text) {
        show(text+1);
        printf("%c", *text);
    }
}
