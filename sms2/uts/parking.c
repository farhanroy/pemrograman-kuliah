#include <stdio.h>
#include <stdlib.h>

#define MAX 3

typedef int type;

typedef struct {
    type item[MAX];
    int count;
    int front;
    int rear;
} Queue;

typedef struct {
    type data[MAX];
    int count;
} Stack;

void initializeQueue();
int isQueueFull();
int isQueueEmpty();
void enqueue();
void dequeue();
void getAllQueue();

void initializeStack();
int isStackEmpty();
int isStackFull();
void push(int);
int pop();
void getAllStack();

void chooseMenu();
void outCar();

Queue queue;
Stack stack;

int main() {
    initializeQueue();
    initializeStack();
    chooseMenu();
    return 0;
}

/*************QUEUE*****************/
void initializeQueue() {
    queue.count = 0;
    queue.front = 0;
    queue.rear = 0;
}

int isQueueFull() {
    return queue.count == MAX;
}

int isQueueEmpty() {
    return queue.count == 0;
}

int isFound(type key) {
    int front = queue.front, rear = queue.rear;

    int count = 0;

    do {
        if (queue.item[front] == key) {
            count++;
            break;
        }
        front = (front + 1) % MAX;
    } while(front != rear);

    return count;
}

void enqueue() {
    if (isQueueFull()) {
        printf("Full\n");
    } else {
        printf("Enter data: ");
        fflush(stdin);
        scanf("%d", &queue.item[queue.rear]);
        queue.rear = (queue.rear+1) % MAX;
        queue.count++;
    }
    chooseMenu();
}

void dequeue() {
    if (isQueueEmpty()) {
        printf("Queue sedang kosong");
    } else {
        type temp = queue.item[queue.front];
        queue.front = (queue.front+1) % MAX;
        queue.count--;
    }
}

void getAllQueue() {
     int front = queue.front, rear = queue.rear;
    if (isQueueEmpty()) {
        printf("Kosong\n");
    } else {
        do {
            printf("%c\n", queue.item[front]);
            front = (front + 1) % MAX;
        } while(front != rear);
    }
    printf("\n");
    chooseMenu();
}

/*************STACK*****************/

void initializeStack() {
    stack.count = 0;
}

int isStackEmpty() {
    return stack.count > 0;
}

int isStackFull() {
    return stack.count == MAX;
}

void push(int data) {
    if (isStackFull()) {
        printf("Sorry your stack is full");
    } else {
        stack.data[stack.count] = data;
        stack.count++;
    }
}

int pop() {
    return stack.data[--stack.count];
}

/******************************/
void chooseMenu() {
    int menu;

    printf("1. Masukan mobil\n2. Keluarkan mobil\n3. Lihat\n4.Exit\n");
    printf("Pilih menu: ");
    scanf("%d", &menu);

    switch(menu) {
        case 1:
            enqueue();
            break;
        case 2:
            outCar();
            break;
        case 3:
            getAllQueue();
            break;
        default:
            exit(0);
    }
}

void outCar() {
    type temp;

    type key;
    printf("Enter key: ");
    scanf("%d", &key);

    if (!isFound(key)) {
        printf("Not found");
    } else {
        // when car in first
        if (queue.item[queue.front] == key) {
            temp = queue.item[queue.front];
        } else {

            while(queue.item[queue.front] != key) {
                push(queue.item[queue.front]);
                queue.front = (queue.front + 1) % MAX;
            }

            temp = queue.item[queue.front];

            while(!isStackEmpty()) {
                queue.item[queue.front] = pop();
                queue.front = (queue.front - 1) % MAX;
            }
        }
    }

    dequeue();
    printf("Pick car %d", queue.item[queue.front]);
    chooseMenu();
}