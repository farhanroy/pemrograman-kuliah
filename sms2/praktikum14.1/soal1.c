#include<stdio.h>
#include<stdlib.h>

struct node {
    char data;
    struct node* left;
    struct node* right;
};

void choose();
struct node* createNode(char);
struct node* find(struct node**, char);
void insert(struct node**, char);
void printPreorder (struct node*);
void printInorder (struct node*);
void printPostorder (struct node*);

int n;
struct node *root;

int main() {
    printf("Masukan jumlah huruf: ");
    scanf("%d", &n);

    char sample[n];
    printf("Isi data sejumah %d item: ", n);
    scanf("%s", sample);

    for (int i = 0; i < n; i++) {
        insert(&root, sample[i]);
    }

    choose();
    
    return 0;
}

void choose() {
    int menu;
    char key, answer1, answer2 = 'y';

    printf("Pilih mode Tree: \n");
    printf("1. Preorder\n2. Inorder\n3. Postorder\npilihan anda[1/2/3]:");
    scanf("%d", &menu);

    switch(menu) {
        case 1: {
            printPreorder(root);
            break;
        }
        case 2: {
            printInorder(root);
            break;
        }
        case 3: {
            printPostorder(root);
            break;
        }
        default: {}
    }

    fflush(stdin);
    printf("\nCoba metode lain [Y/T]: ");
    scanf("%c", &answer1);
    if (answer1 == 'y' || answer1 == 'Y') {
        choose();
    } else {
        while(answer2 == 'y' || answer2 == 'Y') {
            fflush(stdin);
            printf("Data apa yang ingin dicari ? ");
            scanf("%c", &key);

            if (find(&root, key) != NULL) {
                printf("Data ada\n");
            } else {
                printf("Data tidak ada\n");
            }

            fflush(stdin);
            printf("Mau cari lagi [Y/T]: ");
            scanf("%c", &answer2);
        }
        exit(0);
    }
}

struct node* createNode(char data) {
    struct node* newNode = (struct node *) malloc(sizeof(struct node));
    newNode->data = data;
    newNode->left = NULL;
    newNode->right = NULL;
    return (newNode); 
}

void insert(struct node ** node, char data) {

    if ((*node) == NULL) {
        *node = createNode(data);
    } else {
        if (data < (*node)->data) {
             insert(&(*node)->left, data);
        } else if (data > (*node)->data) {
             insert(&(*node)->right, data);
        }
    }
}

struct node* find(struct node** node, char data) {
    if ((*node) == NULL) {
        return NULL;
    } else if ((*node)->data == data) {
        return  (*node);
    } else if (data < (*node)->data) {
        return find(&(*node)->left, data);
    } else if (data > (*node)->data) {
        return find(&(*node)->right, data);
    } else {
        return NULL;
    }
}

void printPreorder (struct node* root) {
    if (root != NULL) {
        printf("%c", root->data);
        printPreorder(root->left);
        printPreorder(root->right);
    }
}

void printInorder (struct node* root) {
    if (root != NULL) {
        printInorder(root->left);
        printf("%c", root->data);
        printInorder(root->right);
    }
}

void printPostorder (struct node* root) {
    if (root != NULL) {
        printPostorder(root->left);
        printPostorder(root->right);
        printf("%c", root->data);
    }
}