#include <stdio.h>
#include <stdlib.h>

#define MAX 3

typedef int type;
typedef struct {
    type item[MAX];
    int count;
    int front;
    int rear;
} Queue;

void initialize(Queue *);
int isFull(Queue *);
int isEmpty(Queue *);
void enqueue(Queue *);
void dequeue(Queue *);
void getAllQueue(Queue *);
void chooseMenu(Queue *);
void getMinMax(Queue *);
void searchData(Queue *);

int main() {
    Queue queue;
    initialize(&queue);
    chooseMenu(&queue);
    return 0;
}

void chooseMenu(Queue *queue) {
    int menu;
    printf("Menu:\n1. Enqueue Data\n2. Dequeue Data\n3. Min & Max\n4. Search Data\n5. Get All\n6. Exit\n");
    printf("Pilihan: ");
    scanf("%d", &menu);
    switch(menu) {
        case 1:
            enqueue(queue);
            break;
        case 2:
            dequeue(queue);
            break;
        case 3:
            getMinMax(queue);
            break;
        case 4:
            searchData(queue);
            break;
        case 5:
            getAllQueue(queue);
            break;
        default:
            exit(0);
    }   
}

void initialize(Queue *queue) {
    queue->count = 0;
    queue->front = 0;
    queue->rear = 0;
}

int isFull(Queue *queue) {
    return queue->count == MAX;
}

int isEmpty(Queue *queue) {
    return queue->count == 0;
}

void enqueue(Queue *queue) {
    if (isFull(queue)) {
        printf("queue is full\n");
    } else {
        printf("Enter data: ");
        fflush(stdin);
        scanf("%d", &queue->item[queue->rear]);
        queue->rear = (queue->rear+1) % MAX;
        queue->count++;
    }
    getAllQueue(queue);
    chooseMenu(queue);
}

void dequeue(Queue *queue) {
    if (isEmpty(queue)) {
        printf("Queue sedang kosong");
    } else {
        type temp = queue->item[queue->front];
        queue->front = (queue->front+1) % MAX;
        queue->count--;
    }

    getAllQueue(queue);
    chooseMenu(queue);
}

void getAllQueue(Queue *queue) {
    int front = queue->front, rear = queue->rear;
    if (isEmpty(queue)) {
        printf("Kosong\n");
    } else {
        do {
            printf("%d\n", queue->item[front]);
            front = (front + 1) % MAX;
        } while(front != rear);
    }
    printf("\n");
    chooseMenu(queue);
}

void getMinMax(Queue *queue) {
    int front = queue->front;
    int rear = queue->rear;

    int min = queue->item[front];
    int max = queue->item[front];

    do {
        if (queue->item[front] > max) {
            max = queue->item[front];
        }

        if (queue->item[front] < min) {
            min = queue->item[front];
        }
        front = (front + 1) % MAX;
    } while(front != rear);

    printf("Max = %d, Min = %d\n", max, min);
    chooseMenu(queue);
}

void searchData(Queue *queue) {
    int front = queue->front, rear = queue->rear;

    type key;
    printf("Masukan key: ");
    scanf("%d", &key);

    int count = 0;

    do {
        if (queue->item[front] == key) {
            count++;
        }
        front = (front + 1) % MAX;
    } while(front != rear);

    if (count == 0) {
        printf("Data tidak ditemukan \n");
    } else {
        printf("Data ditemukan %d kali\n", count);
    }
    chooseMenu(queue);
}