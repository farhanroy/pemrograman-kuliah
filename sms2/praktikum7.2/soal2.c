#include<stdio.h>
#include<stdlib.h>

typedef struct node Node;

struct node {
    int data;
    char priority;
    Node *next;
};

void createNewNode();
void insert();
void delete();
void getAll();

Node *newNode;
Node *front;

int main() {
    int choice, data, priority;  
    do  
    {  
        printf("1.Insert\n");  
        printf("2.Delete\n");  
        printf("3.Display\n");  
        printf("4.Quit\n");  
        printf("Enter your choice : ");  
        scanf("%d", &choice);  
        switch(choice)  
        {  
            case 1:  
                insert();  
                break;  
            case 2:  
                delete();  
                break;  
            case 3:  
                getAll();  
                break;  
            case 4:  
            break;  
                default :  
                printf("Wrong choice\n");  
        }  
    }while(choice!=4);  
  
    return 0;  
}

void createNewNode() {
    newNode = (Node *) malloc(sizeof(Node));
    
    if (newNode == NULL) {
        printf("Cannot allocate memory");
        exit(0);
    } else {
        printf("Enter value for new node: ");
        scanf("%d", &newNode->data);
        fflush(stdin);
        printf("Enter priority too: ");
        scanf("%c", &newNode->priority);
        fflush(stdin);
        newNode->next = NULL;
    }
}

void insert() {
    Node *current;
    createNewNode();

    if( front == NULL || newNode->priority < front->priority )  {  
        newNode->next = front;  
        front = newNode;  
    }  
    else {  
        current = front;  
        while(current->next != NULL && current->next->priority <= newNode->priority) {
            current = current->next;
        }
            
        newNode->next = current->next;  
        current->next = newNode;  
    }  
}

void delete()  
{  
    Node *temp;  
    if(front == NULL)  
        printf("Queue Kosong\n");  
    else {  
        temp = front;  
        printf("Deleted item is %d\n", temp->data);  
        front = front->next;  
        free(temp);  
    }  
}

void getAll()  
{  
    Node *current;  
    current = front;  
    if(front == NULL)  
        printf("Queue is empty\n");  
    else {     
        printf("Priority       Item\n");  
        while(current != NULL)  
        {  
            printf("%c        %d\n",current->priority,current->data);  
            current = current->next;  
        }  
    }  
}  