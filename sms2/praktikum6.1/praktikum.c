#include<stdio.h>
#include<stdlib.h>

#define MAX 5

typedef char type;

typedef struct {
    type data[MAX];
    int count;
} Stack;

void chooseMenu(Stack *);

void initialize(Stack *);
int isEmpty(Stack *);
int isFull(Stack *);
void push(Stack *);
void pop(Stack *);
void getAllData(Stack *);

int main() {
    Stack stack;

    initialize(&stack);
    chooseMenu(&stack);

    return 0;
}

void chooseMenu(Stack *stack) {
    int menu;
    printf("Menu:\n1. Push Data\n2. Pop Data\n3. Get All\n4. Exit\n");
    printf("Pilihan: ");
    scanf("%d", &menu);
    switch(menu) {
        case 1:
            push(stack);
            break;
        case 2:
            pop(stack);
            break;
        case 3:
            getAllData(stack);
            break;
        default:
            exit(0);
    }   
}

void initialize(Stack *stack) {
    stack->count = 0;
}

int isEmpty(Stack *stack) {
    if (stack->count > 0) 
        return 0;
    else
        return 1;
}

int isFull(Stack *stack) {
    if (stack->count == MAX) 
        return 1;
    else
        return 0;
}

void push(Stack *stack) {
    if (isFull(stack)) {
        printf("Sorry your stack is full");
    } else {
        printf("Enter data: ");
        fflush(stdin);
        scanf("%c", &stack->data[stack->count]);
        stack->count++;
        getAllData(stack);
    }
    chooseMenu(stack);
}

void pop(Stack *stack) {
    if (isEmpty(stack)) {
        printf("Sorry stack is empty\n");
    } else {
        --stack->count;
        getAllData(stack);
    }
    chooseMenu(stack);
}

void getAllData(Stack *stack) {
    for (int i = stack->count-1; i >= 0 ; i--) {
        printf("%c\n", stack->data[i]);
    }
    chooseMenu(stack);
}



