#include <stdio.h>
#include <string.h>
#define MAXS 23

typedef struct {
    char data[MAXS];
    int count;
} stack;

stack docker;

int palindrom(char []);

void main() {
    char sentence[MAXS];
    char again;
    puts("MENGECEK PALINDROM");

    do {
        printf("Masukkan kata yang dicek : ");
        gets(sentence);
        fflush(stdin);

        if(palindrom(sentence) == 0)
            printf("%s adalah BUKAN PALINDROM\n", sentence);
        else
            printf("%s adalah PALINDROM\n", sentence);


        printf("\n\nMau coba lagi (y/t) ? "); scanf("%c", &again); fflush(stdin);
    } while(again == 'Y' || again == 'y');
}

int full() {
    return docker.count == MAXS;
}

int empty() {
    return docker.count == 0;
}

void push(char sentence) {
    if(!full())
        docker.data[docker.count++] = sentence;
}

char pop() {
    if(!empty())
        return docker.data[docker.count--];
}

int palindrom(char sentence[MAXS]) {
    /* Input to data (stack) with call function push */
    for(int i = 0; i < strlen(sentence); i++)
        push(sentence[i]);

    /* Compare the sentence with reverse sentence (calling function pop) */
    for(int i = 0; i < strlen(sentence); i++)
        if(sentence[i] == pop())
            if(empty())
                return 1;
        else
            return 0;
}