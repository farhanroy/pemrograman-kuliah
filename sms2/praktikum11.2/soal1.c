#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
#define MAX 10
 
typedef struct {
    int no;
    char nama[50];
    int nilai;
} Data;
 
void choose();
void input(Data []);
void show(Data []);
void copy(Data [], Data []);
void insertion(Data [], int, int);
void selection(Data [], int, int);
void bubble(Data [], int, int);
void shell(Data [], int, int);
void swap(Data *, Data *);
 
Data data[MAX];
 
int n;
 
int main() {
    input(data);
    choose(data);
    return 0;
}
 
void choose() {
    Data temp[n];
    int menu, mode, by;
 
    copy(data, temp);
 
    printf("Pilih menu\n");
    printf("1. Insertion\n2. Selection\n3. Bubble\n4. Shell\n5. Exit\n");
 
    printf("Masukan menu: ");
    scanf("%d", &menu);
 
    if (menu < 5) {
        printf("Urut berdasarkan:\n");
        printf("1. No\n2. Nama\n3. Nilai\n");
        printf("Masukan [1/2/3]: ");
        scanf("%d", &by);

        printf("Pilih mode\n");
        printf("1. Ascending\n2. Descending\n");
        printf("Masukan mode: ");
        scanf("%d", &mode);
    }
    
    switch(menu) {
        
        case 1:
            insertion(temp, mode, by);
            break;
        case 2:
            selection(temp, mode, by);      
            break;
        case 3:
            bubble(temp, mode, by);
            break;
        case 4:
            shell(temp, mode, by);
            break;
        default:
            exit(0);
    }
 
    show(temp);
    choose();
}
 
void input(Data data[]) {
    printf("Berapa jumlah siswa: ");
    scanf("%d", &n);
 
    for (int i = 0; i < n; i++) {
        printf("Masukan no: ");
        scanf("%d", &data[i].no);
        fflush(stdin);
        printf("Masukan nama: ");
        scanf("%[^\n]%*c", data[i].nama);
        fflush(stdin);
        printf("Masukan nilai: ");
        scanf("%d", &data[i].nilai);
        fflush(stdin);
    }
}
 
void insertion(Data d[], int mode, int by) {
    int i, j, kondisi;
    Data key;
    for(i = 1; i < n; i++) {
        key = d[i];
        j = i-1;
        
        if (by == 1) {
            if (mode == 1) {
                kondisi = d[j].no > key.no;
            } else {
                kondisi = d[j].no < key.no;
            }
        } else if (by == 2) {
            if (mode == 1) {
                kondisi = strcmp(d[j].nama, key.nama) > 0;
            } else {
                kondisi = strcmp(d[j].nama, key.nama) < 0;
            }
        } else {
            if (mode == 1) {
                kondisi = d[j].nilai > key.nilai;
            } else {
                kondisi = d[j].nilai < key.nilai;
            }
        }


        while(j >= 0 && kondisi) {
            d[j+1] = d[j];
            j--;
        }

        d[j+1] = key;
    }
}
 
 
void selection(Data d[], int mode, int by) {
    int i, j, min, kondisi;
    for(i = 0; i < n - 1; i++) {
        min = i;
        for(j = i+1; j < n; j++)  {
            if (by == 1) {
                if (mode == 1) {
                    kondisi = d[j].no < d[min].no;
                } else {
                    kondisi = d[j].no > d[min].no;
                }
            } else if (by == 2) {
                if (mode == 1) {
                    kondisi = strcmp(d[j].nama, d[min].nama) < 0;
                } else {
                    kondisi = strcmp(d[j].nama, d[min].nama) > 0;
                }
            } else {
                if (mode == 1) {
                    kondisi = d[j].nilai < d[min].nilai;
                } else {
                    kondisi = d[j].nilai > d[min].nilai;
                }
            }

            if (kondisi) 
                min = j;
            
        }
        swap(&d[min], &d[i]);
    }
}
 
void bubble(Data d[], int mode, int by)
{
    int i, j, flag = 1, r = 0, kondisi;
    for (i = 0; i < n - 1 && flag ; i++) {
            flag = 0;
            for (j = 0; j < n - i - 1; j++) {
                if (by == 1) {
                    if (mode == 1) {
                        kondisi = d[j].no > d[j+1].no;
                    } else {
                        kondisi = d[j].no < d[j+1].no;
                    }
                } else if (by == 2) {
                    if (mode == 1) {
                        kondisi = strcmp(d[j].nama, d[j+1].nama) > 0;
                    } else {
                        kondisi = strcmp(d[j].nama, d[j+1].nama) < 0;
                    }
                } else {
                    if (mode == 1) {
                        kondisi = d[j].nilai > d[j+1].nilai;
                    } else {
                        kondisi = d[j].nilai < d[j+1].nilai;
                    }
                }
 
                if (kondisi) {
                    swap(&d[j], &d[j + 1]);
                    flag = 1;
                }
            }
    }
                
}
 
void shell(Data d[], int choose, int orderBy) {
    int condition, flag;
    int subList = n;
    while(subList > 1) {
        subList /= subList;
        flag = 1;
        while(flag == 1) {
            flag = 0;
            for(int i = 0; i < n-subList; i++) {
                  if(choose == 1) {
                        if(orderBy == 1) {
                            condition = d[i].no > d[i+subList].no;
                        }
                            
                        else if(orderBy == 2) {
                            condition = strcmp(d[i].nama, d[i+subList].nama) > 0;
                        }
                            
                        else {
                            condition = d[i].nilai > d[i+subList].nilai;
                        }
                            
                    } else {
                        if(orderBy == 1) {
                            condition = d[i].no < d[i+subList].no;
                        }
                    
                        else if(orderBy == 2) {
                            condition = strcmp(d[i].nama, d[i+subList].nama) < 0;
                        }
                            
                        else {
                            condition = d[i].nilai < d[i+subList].nilai;
                        }
                            
                    }
               
                    if (condition) {
                        swap(&d[i], &d[i+subList]);
                        flag = 1;
                    }
            }
        }
    }

    
}


 
void show(Data d[]) {
    for (int i = 0; i < n; i++) {
         printf("%d %s %d\n", d[i].no, d[i].nama, d[i].nilai);
    }
}
 
void copy(Data arr[], Data copy[]) {
  for (int i = 0; i < n; i++) {
    copy[i].no = arr[i].no;
    copy[i].nilai = arr[i].nilai;
    strcpy(copy[i].nama, arr[i].nama);
  }
}
 
void swap(Data *xp, Data *yp)
{
    Data temp = *xp;
    *xp = *yp;
    *yp = temp;
}