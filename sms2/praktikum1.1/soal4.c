#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAKS 200

typedef struct {
    char nama[50];
    int tugas;
    int uts;
    int uas;
} nilai;

void input(nilai *);
void tampil(nilai *);
float nilaiAkhir(int, int, int);
char getGrade(int);

int n;

int main()
{
    nilai data[MAKS];
    printf("\tMENGHITUNG NILAI\t\t\n");
	printf("Mata Kuliah Konsep Pemrograman\n\n");
	
	input(data);
	tampil(data);
    return 0;
}

void input(nilai *data) {
    printf("Berapa mahasiswa: ");
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        printf("Input mahasiswa ke-%d\n", i+1);
        fflush(stdin);

        printf("Masukan nama: ");
        gets(data->nama);
        fflush(stdin);

        printf("Masukan nilai tugas: ");
        scanf("%d", &data->tugas);
        fflush(stdin);

        printf("Masukan nilai uts: ");
        scanf("%d", &data->uts);
        fflush(stdin);

        printf("Masukan nilai uas: ");
        scanf("%d", &data->uas);
        fflush(stdin);

        data++;
    }
}

void tampil(nilai *data) {
    char namaTertinggi[50];
    float nilaiAkhirTertinggi = 0;

    printf("\t\tDaftar Nilai\t\t\n");
    printf("\tMata Kuliah Konsep Pemrograman\t\n");

    printf("-------------------------------------------------------------\n");
    printf("No\tNama Mahasiswa\tTugas\tUTS\tUAS\tAkhir\tGrade\n");
    printf("-------------------------------------------------------------\n");

    

    for (int i = 0; i < n; i++) {
        float akhir = nilaiAkhir(data->tugas, data->uts, data->uas);
        char grade = getGrade(akhir);


        if (nilaiAkhirTertinggi < akhir) {
            strcpy(namaTertinggi, data->nama);
            nilaiAkhirTertinggi = akhir;
        }

        printf("%d\t%s\t\t%d\t%d\t%d\t%.1f\t%c\n", i+1, data->nama, data->tugas, data->uts, data->uas, akhir, grade);
        data++;
    }

    printf("-------------------------------------------------------------\n");

    printf("Mahasiswa dengan nilai tertinggi \n");
    printf("Nama: %s\n", namaTertinggi);
    printf("Nilai: %.1f", nilaiAkhirTertinggi);
}

float nilaiAkhir(int tugas, int uts, int uas) {
    float nilai =  (0.2 * tugas) + (0.4 * uts) + (0.4 * uas);
    return nilai;
}

char getGrade(int akhir) {
    char grade;
        if(akhir>=80)
            grade = 'A';
        else if(akhir>=70)
            grade = 'B';
        else if(akhir>=60)
            grade = 'C';
        else if(akhir>=50)
            grade = 'D';
        else
            grade = 'E';
    
    return grade;
}

