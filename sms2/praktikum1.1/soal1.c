#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAKS 200

struct nilai{
    char nama[50];
    int tugas;
    int uts;
    int uas;
};

void input(struct nilai []);
void tampil(struct nilai []);
int nilaiAkhir(int, int, int);
char getGrade(int);

int n;

int main()
{
    struct nilai data[MAKS];
    printf("\tMENGHITUNG NILAI\t\t\n");
	printf("Mata Kuliah Konsep Pemrograman\n\n");
	
	input(data);
	tampil(data);
    return 0;
}

void input(struct nilai data[]) {
    printf("Berapa mahasiswa: ");
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        printf("Input mahasiswa ke-%d\n", i+1);
        fflush(stdin);

        printf("Masukan nama: ");
        scanf("%s", &data[i].nama);
        fflush(stdin);

        printf("Masukan nilai tugas: ");
        scanf("%d", &data[i].tugas);
        fflush(stdin);

        printf("Masukan nilai uts: ");
        scanf("%d", &data[i].uts);
        fflush(stdin);

        printf("Masukan nilai uas: ");
        scanf("%d", &data[i].uas);
        fflush(stdin);
    }
}

void tampil(struct nilai data[]) {
    printf("\t\tDaftar Nilai\t\t\n");
    printf("\tMata Kuliah Konsep Pemrograman\t\n");

    printf("-------------------------------------------------------------\n");
    printf("No\tNama Mahasiswa\tTugas\tUTS\tUAS\tAkhir\tGrade\n");
    printf("-------------------------------------------------------------\n");

    for (int i = 0; i < n; i++) {
        int akhir = nilaiAkhir(data[i].tugas, data[i].uts, data[i].uas);
        char grade = getGrade(akhir);
        printf("%d\t%s\t\t%d\t%d\t%d\t%d\t%c\n", i+1, data[i].nama, data[i].tugas, data[i].uts, data[i].uas, akhir, grade);
    }

    printf("-------------------------------------------------------------\n");
}

int nilaiAkhir(int tugas, int uts, int uas) {
    int nilai =  (0.2 * tugas) + (0.4 * uts) + (0.4 * uas);
    return nilai;
}

char getGrade(int akhir) {
    char grade;
        if(akhir>=80)
            grade = 'A';
        else if(akhir>=70)
            grade = 'B';
        else if(akhir>=60)
            grade = 'C';
        else if(akhir>=50)
            grade = 'D';
        else
            grade = 'E';
    
    return grade;
}

