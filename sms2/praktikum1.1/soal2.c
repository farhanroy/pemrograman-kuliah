#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int n;

typedef struct {
    int d, m, y;
} Date;

typedef struct{
    int gaji;
    char name[23], gender;
    Date birthDate;
} karyawan;

void input(karyawan *);
void tampil(karyawan *);

int main() {
    karyawan data[23];
    printf("\t\tData Karyawan\n");
    
    input(data);
    tampil(data);
}

void input(karyawan *data) {
    printf("\nMasukan jumlah karyawan: "); 
    scanf("%d", &n); 
    fflush(stdin);

    for(int i=0; i<n; i++){
        printf("\nMahasiswa ke-%d", i+1); 

        printf("\nMasukan nama: "); 
        gets(&data->name);

        printf("Tanggal lahir (hari-bulan-tahun): "); 
        scanf("%d-%d-%d", &data->birthDate.d, &data->birthDate.m, &data->birthDate.y); 
        fflush(stdin);

        printf("Jenis kelamain [L/P]: "); 
        scanf("%c", &data->gender); 
        fflush(stdin);

        printf("Gaji perbulan: "); 
        scanf("%d", &data->gaji); 
        fflush(stdin); 
        
        data++;
    }
}

void tampil(karyawan *data) {
    puts("\n\n\tData Karyawan\n");
    for (int i=0; i<n; i++) {
        
        printf("Nama: %s\n", data->name);
        printf("Tanggal lahir: %d-%d-%d\n", data->birthDate.d, data->birthDate.m, data->birthDate.y);
        switch (data->gender){
            case 'L': case 'p': printf("Jenis Kelamin: Laki-Laki"); break;
            case 'P': case 'l': printf("Jenis Kelamin: Perempuan"); break;
        }
        printf("Gaji perbulan: Rp %d\n\n", data->gaji); 
        data++;
    }
}