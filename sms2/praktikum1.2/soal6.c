#include<stdio.h>

typedef struct {
    int d, m, y;
} Date;

typedef struct {
    char name[60];
    double averageScore;
    Date dob;
} Student;

struct Class {
    Student stud[30];
    int numStud;
};

int isYounger(Student, Student);

int main() {

    Student student1 = {"Roy", 90.0, {22, 11, 2000}};
    Student student2 = {"Farchan", 90.0, {20, 11, 2000}};

    if (isYounger(student1, student2) == 1) {
        printf("%s lebih muda dari %s", student1.name, student2.name);
    } else if (isYounger(student1, student2) == 0) {
        printf("%s lebih muda dari %s", student2.name, student1.name);
    } else {
        printf("%s umurnya sama dengan %s", student2.name, student1.name);
    }

    return 0;
}

int isYounger(Student s1, Student s2) {
    if (s1.dob.y > s2.dob.y) {
        return 1;
    } else if (s1.dob.y < s2.dob.y) {
        return 0;
    } else if (s1.dob.m > s2.dob.m) {
        return 1;
    } else if (s1.dob.m < s2.dob.m) {
        return 0;
    } else if (s1.dob.d > s2.dob.d) {
        return 1;
    } else if (s1.dob.d < s2.dob.d) {
        return 0;
    } else {
        return -1;
    }
}