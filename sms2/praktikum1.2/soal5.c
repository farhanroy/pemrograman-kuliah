#include<stdio.h>

typedef struct {
    int day, month, year;
} Date;

int isDateSame(Date, Date);

int main() {

    Date date1 = {20, 2, 2022};
    Date date2 = {21, 2, 2022};

    if (isDateSame(date1, date2)) {
        printf("Tanggal 1 dan 2 sama");
    } else {
        printf("Tanggal 1 dan 2 tidak sama");
    }

    return 0;
}

int isDateSame(Date d1, Date d2) {
    if ((d1.day == d2.day) && (d1.month == d2.month) && (d1.year == d2.year)) {
        return 1;
    } else {
        return 0;
    }
}