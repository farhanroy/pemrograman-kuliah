#include<stdio.h>
#include<stdlib.h>

#define max 20

typedef struct siswa Node;

struct siswa {
    int no;
    char nama[max];
    float nilai;
    Node *next;
};

Node *head;
Node *newNode;

void chooseMenu();
void chooseMenuInsert();
void chooseMenuDelete();

void processMenuInsert(int);
void processMenuDelete(int);

void createNewNode();
void clearNode(Node *);
void getAllNode();

void insertFirst();
void insertLast();
void insertAfter();
void insertBefore();

void deleteFirst();
void deleteLast();
void deleteCertain();

void programExit();

int main() {
    chooseMenu();
}

void chooseMenu() {
    int menu;
    printf("Menu:\n");
    printf("1. Insert\n2. Delete\n3. Exit\n");
    printf("Pilih menu: ");
    scanf("%d", &menu);

    if (menu == 1) 
        chooseMenuInsert();
    else if (menu == 2)
        chooseMenuDelete();
    else 
        programExit();
}

void chooseMenuInsert() {
    int menu;
    printf("Menu:\n");
    printf("1. Awal\n2. Akhir\n3. After\n4. Before\n");
    printf("Pilih menu: ");
    scanf("%d", &menu);
    processMenuInsert(menu);
}

void chooseMenuDelete() {
    int menu;
    printf("Menu:\n");
    printf("1. Awal\n2. Akhir\n3. Tertentu\n");
    printf("Pilih menu: ");
    scanf("%d", &menu);
    processMenuDelete(menu);
}

void processMenuInsert(int menu) {
    char answer = 'y';

    while(answer == 'y' || answer == 'Y') {
        if (menu == 1) {
            insertFirst();
        } else if (menu == 2) {
            insertLast();
        } else if (menu == 3) {
            insertAfter();
        } else if (menu == 4) {
            insertBefore();
        } else {
            programExit();
        }

        printf("Are you input again (y/n) ? ");
        fflush(stdin);
        scanf("%c", &answer);
    }
    
    getAllNode();
    chooseMenu();
}

void processMenuDelete(int menu) {
    if (menu == 1) {
            deleteFirst();
        } else if (menu == 2) {
            deleteLast();
        } else if (menu == 3) {
            deleteCertain();
        } else {
            programExit();
        }
}

void createNewNode() {
    newNode = (Node *) malloc(sizeof(Node));
    
    if (newNode == NULL) {
        printf("Cannot allocate memory");
        exit(0);
    } else {
        printf("Enter value for new node: \n");

        printf("No: ");
        fflush(stdin);
        scanf("%d", &newNode->no);

        printf("Name: ");
        fflush(stdin);
        scanf("%[^\n]s",newNode->nama);

        printf("Value: ");
        fflush(stdin);
        scanf("%f", &newNode->nilai);

        newNode->next = NULL;
    }
}

void clearNode(Node *n) {
    free(n);
    n = NULL;
}

void getAllNode() {
    Node *temp = head;
    printf("\nNo\tName\tValue\n");
    while (temp != NULL) {
        printf("%d\t%s\t%.1f\n", temp->no, temp->nama, temp->nilai);
        temp = temp->next;
    }
}

void insertFirst() {
    createNewNode();

    if (head != NULL) {
        newNode->next = head;
    } 

    head = newNode;
}

void insertLast() {
    Node *current;

    createNewNode();

    if (head == NULL) {
        head = newNode;
    } else {
        current = head;
        while(current->next != NULL) {
            current = current->next;
        }
        // add newNode in current->next
        current->next = newNode;
        // change current state to last node
        current = current->next;
    }
}

void insertAfter() {
    int key;
    Node *current;

    createNewNode();

    if (head == NULL) {
        head = newNode;
    } else {

        printf("Append number after: ");
        scanf("%d", &key);

        current = head;
        while(current->no != key) {
            if (current->next == NULL) {
                printf("Number not found");
                exit(0);
            } else {
                current = current->next;
            }
        }

        newNode->next = current->next;
        current->next = newNode;
    }
}

void insertBefore() {
    int key;
    Node *current;
    Node *previous;

    createNewNode();

    if (head == NULL) {
        head = newNode;
    } else {

        printf("Append number before: ");
        scanf("%d", &key);

        if (head->no == key) {
            newNode->next = head;
            head = newNode;
        } else {
            current = head;
            while(current->no != key) {
                if (current->next == NULL) {
                    printf("Key not found");
                    exit(0);
                } else {
                    previous = current;
                    current = current->next;
                }
            }

            previous->next = newNode;
            newNode->next = current;
        }

    }
}


/*
* Single Linked List Delete
*/

void deleteFirst() {
    Node *current;

    current = head;
    if (head == NULL) {
        programExit();
    } else if(head->next == NULL) {
        head = NULL;
    } else {
        head = current->next;
    }

   clearNode(current);

    getAllNode();
    chooseMenu();
}

void deleteLast() {
    Node *current, *temp;

    current = head;
    if (head == NULL) {
        programExit();
    } else if (head->next == NULL) {
        head = NULL;
    } else {
        while(current->next != NULL) {
            temp = current;
            current = current->next;
        }
        temp->next = NULL;
    }

    clearNode(current);

    getAllNode();
    chooseMenu();
}

void deleteCertain() {
    Node *current, *temp;
    int key;

    printf("Masukan key: ");
    scanf("%d", &key);

    current = head;
    if (head == NULL) {
        programExit();
    } else if (head->no == key) {
        deleteFirst();
    } else {
        while(current->no != key) {
            if (current->next == NULL) {
                programExit();
            } else {
                temp = current;
                current = current->next;
            }
        }
        temp->next = current->next;
    }

    clearNode(current);

    getAllNode();
    chooseMenu();
}


void programExit() {
    printf("\nProgram keluar\n");
    exit(0);
}