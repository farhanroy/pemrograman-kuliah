/*
* SELECTION
*/
#include<stdio.h>

#define MAX 10

void selection(int []);
void show(int []);
void swap(int *, int *);

int main() {
    int arr[] = { 3, 10, 4, 6, 8, 9, 7, 2, 1, 5 };
    printf("Selection\n");
    printf("Sebelum\n");
    show(arr);
    selection(arr);
    printf("Sesudah\n");
    show(arr);
    return 0;
}

void selection(int arr[]) {
    int i, j, min;
    for(i = 0; i < MAX; i++) {
        min = i;
        for(j = i; j < MAX; j++)  {
            if (arr[j] < arr[min]) {
                min = j;
            }
        }
        swap(&arr[min], &arr[i]);
    }
}

void show(int arr[]) {
    for (int i = 0; i < 10; i++) {
         printf("%d ", arr[i]);
    }
    printf("\n");
}

void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}