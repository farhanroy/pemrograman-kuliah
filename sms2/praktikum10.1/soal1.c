/*
* INSERTION
*/
#include<stdio.h>

#define MAX 10

void insertion(int []);
void show(int []);

int main() {
    int arr[] = { 3, 10, 4, 6, 8, 9, 7, 2, 1, 5 };
    printf("Insertion\n");
    printf("Sebelum\n");
    show(arr);
    insertion(arr);
    printf("Sesudah\n");
    show(arr);
    return 0;
}

void insertion(int arr[]) {
    int i, j, key;
    for(i = 1; i < MAX; i++) {
        key = arr[i];
        j = i-1;
        while(j >= 0 && arr[j] > key) {
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = key;
    }
}

void show(int arr[]) {
    for (int i = 0; i < 10; i++) {
         printf("%d ", arr[i]);
    }
    printf("\n");
}