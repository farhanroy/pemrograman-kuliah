#include<stdio.h>
#include<stdlib.h>

#define MAX 10

void chooseMenu();
void insertion(int [], int);
void selection(int [], int);
void show(int []);
void swap(int *, int *);
void copyArray(int [], int [], int);

int main() {
    
    chooseMenu();
    return 0;
}

void chooseMenu() {
    int menu, mode;
    int arr[MAX] = { 3, 10, 4, 6, 8, 9, 7, 2, 1, 5 };
    int temp[MAX];

    printf("Pilih menu\n");
    printf("1. Insertion\n2. Selection\n3. Exit\n");

    printf("Masukan menu: ");
    scanf("%d", &menu);

    if (menu != 3) {
        printf("Pilih mode\n");
        printf("1. Ascending\n2. Descending\n");
        printf("Masukan mode: ");
        scanf("%d", &mode);
    }
    copyArray(arr, temp, MAX);
    switch(menu) {
        
        case 1:
            insertion(temp, mode);
            break;
        case 2:
            selection(temp, mode);      
            break;
        default:
            exit(0);
    
    }

    show(temp);
}

void insertion(int arr[], int mode) {
    int i, j, key;
    for(i = 1; i < MAX; i++) {
        key = arr[i];
        j = i-1;
        if (mode == 1) {
            while(j >= 0 && arr[j] > key) {
                arr[j+1] = arr[j];
                j--;
            }
        } else {
            while(j >= 0 && arr[j] < key) {
                arr[j+1] = arr[j];
                j--;
            }
        }
        arr[j+1] = key;
    }
}

void selection(int arr[], int mode) {
    int i, j, min;
    for(i = 0; i < MAX - 1; i++) {
        min = i;
        for(j = i+1; j < MAX; j++)  {
            if (mode == 1) {
                if (arr[j] < arr[min]) {
                    min = j;
                }
            } else {
                if (arr[j] > arr[min]) {
                    min = j;
                }
            }
            
        }
        swap(&arr[min], &arr[i]);
    }
}

void show(int arr[]) {
    for (int i = 0; i < 10; i++) {
         printf("%d ", arr[i]);
    }
    printf("\n");
    chooseMenu();
}

void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void copyArray(int arr[], int copy[], int size) {
  for (int i = 0; i < size; ++i) {
    copy[i] = arr[i];
  }
}