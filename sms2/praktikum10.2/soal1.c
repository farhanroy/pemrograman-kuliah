#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 10

typedef struct {
    int no;
    char nama[50];
    int nilai;
} Data;

void choose();
void input(Data []);
void show(Data []);
void copy(Data [], Data []);
void insertion(Data [], int);
void selection(Data [], int);
void swap(Data *, Data *);

Data data[MAX];

int n;

int main() {
    input(data);
    choose(data);
    return 0;
}

void choose() {
    Data temp[n];
    int menu, mode;

    copy(data, temp);

    printf("Pilih menu\n");
    printf("1. Insertion\n2. Selection\n3. Exit\n");

    printf("Masukan menu: ");
    scanf("%d", &menu);

    if (menu != 3) {
        printf("Pilih mode\n");
        printf("1. Ascending\n2. Descending\n");
        printf("Masukan mode: ");
        scanf("%d", &mode);
    }
    
    switch(menu) {
        
        case 1:
            insertion(temp, mode);
            break;
        case 2:
            selection(temp, mode);      
            break;
        default:
            exit(0);
    }

    show(temp);
    choose();
}

void input() {
    printf("Berapa jumlah siswa: ");
    scanf("%d", &n);

    for (int i = 0; i < n; i++) {
        printf("Masukan no: ");
        scanf("%d", &data[i].no);
        printf("Masukan nama: ");
        scanf("%s", &data[i].nama);
        printf("Masukan nilai: ");
        scanf("%d", &data[i].nilai);
    }
}

void insertion(Data d[], int mode) {
    int i, j;
    Data key;
    for(i = 1; i < n; i++) {
        key = d[i];
        j = i-1;
        if (mode == 1) {
            while(j >= 0 && d[j].no > key.no) {
                d[j+1] = d[j];
                j--;
            }
        } else {
            while(j >= 0 && d[j].no < key.no) {
                d[j+1] = d[j];
                j--;
            }
        }
        d[j+1] = key;
    }
}


void selection(Data arr[], int mode) {
    int i, j, min;
    for(i = 0; i < n - 1; i++) {
        min = i;
        for(j = i+1; j < n; j++)  {
            if (mode == 1) {
                if (arr[j].no < arr[min].no) {
                    min = j;
                }
            } else {
                if (arr[j].no > arr[min].no) {
                    min = j;
                }
            }
            
        }
        swap(&arr[min], &arr[i]);
    }
}

void show(Data d[]) {
    for (int i = 0; i < n; i++) {
         printf("%d %s %d\n", d[i].no, d[i].nama, d[i].nilai);
    }
}

void copy(Data arr[], Data copy[]) {
  for (int i = 0; i < n; i++) {
    copy[i].no = arr[i].no;
    copy[i].nilai = arr[i].nilai;
    strcpy(copy[i].nama, arr[i].nama);
  }
}

void swap(Data *xp, Data *yp)
{
    Data temp = *xp;
    *xp = *yp;
    *yp = temp;
}