#include <stdio.h>
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////     PENDAHULUAN                                        //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
void pendahuluan1() {
    printf("Nama : M. Roy Farchan\n");
    printf("NRP : 3121600028\n");
    printf("Kelas : D4 IT A\n");
}

void pendahuluan2() {
    printf("%d", 1024+4096);
}

void pendahuluan3() {
   int a = 1;
   int b = 2;
   printf("Masukan nilai a = ");
   scanf("%d", &a);
   printf("Masukan nilai b = ");
   scanf("%d", &a);

   int c = a + b;
   printf("%d + %d = %d", a, b, c);
}

void pendahuluan4() {
    float a;
    printf("Masukan nilai a = ");
    scanf("%f", &a);
    printf("Hasil %f * 50 = %f", a, a*50.0);
}

void pendahuluan5() {
    printf("OUTPUT = %f", 0.5);
}

void pendahuluan6() {
    char karakter;
    int jumlah;

    printf("Masukan karakter: ");
    scanf("%c", &karakter);

    printf("Masukan jumlah: ");
    scanf("%d", &jumlah);

    printf("Karakter = %c \n Jumlah = %d", karakter, jumlah);
}


//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////     SOAL                                               //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

void soalA() {
    int jawab, hasil;
    jawab = 100;
    hasil = jawab - 10;

    printf("Jawabannya adalah %d\n", hasil + 6);
}

void soalB() {
    int value1, value2, sum;
    value1 = 35;
    value2 = 18;
    sum = value1 + value2;

    printf("The sum of %d and %d is %d\n", value1, value2, sum);
}

void soalC() {
    int jumlah;

    jumlah = 25+37-19;
    printf("Berapa hasil perhitungan 25 + 37 - 19 ?\n");
    printf("Jawabannya adalah %d \n", jumlah);


}

void soalD() {
    int bil1 = 10;
    int bil2  = 5;

    int jumlah = bil1 + bil2;
    int rata = (bil1+bil2)/2;

    int kuadrat1 = bil1*bil1;
    int kuadrat2 = bil2*bil2;

    printf("%d + %d = %d", bil1, bil2, jumlah);
    printf("rata dari %d dan %d = %d", bil1, bil2, rata);

    printf("%d pangkat 2 = ", bil1, kuadrat1);
    printf("%d pangkat 2 = ", bil2, kuadrat2);
}

void soalE() {
    float a, b, c;

    a = 3.0;
    b = 4.0;

    c = (a*a) + (b*b);

    printf("c*c = %f", c);
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////                        MAIN                            //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

int main() {

    soal1();

    return 0;
}
