#include <stdio.h>
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////     PENDAHULUAN                                        //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

/*

*/
void pendahuluan1() {
    int a;

    printf("Masukan nilai a = ");
    scanf("%d", &a);
    
    if (a >= 0) {
        printf("Masukan nilai a adalah positif ");
    } else {
        printf("Masukan nilai a adalah negatif ");
    }
}

void pendahuluan2() {
    int a;

    printf("Masukan bilangan a = ");
    scanf("%d", &a);
    
    if (a % 2 == 0) {
        printf("Bilangan a adalah genap");
    } else {
        printf("Bilangan a adalah ganjil");
    }
}

void pendahuluan3() {
        int totalPembelian;
    float diskon;

    printf("Masukan total pembelian = ");
    scanf("%d", &totalPembelian);
    
    if (totalPembelian > 100000) {
        diskon = 0.05;
    } else {
        diskon = 0;
    }
    
    totalPembelian -= totalPembelian * diskon;
    printf("Total pembelian anda setelah didiskon = %d", totalPembelian);

}

void pendahuluan4() {
        int bilanganPertama, bilanganKedua;

    printf("Masukan bilangan pertama = ");
    scanf("%d", &bilanganPertama);
    
    printf("Masukan bilangan kedua = ");
    scanf("%d", &bilanganKedua);
    
    if (bilanganPertama % bilanganKedua == 0) {
        printf("Bilangan pertama adalah kelipatan persekutuan bilangan kedua");
    } else {
        printf("Bilangan pertama bukan kelipatan persekutuan bilangan kedua");
    }

}

void pendahuluan5() {
    float bil1, bil2, hasil;

    printf("Masukan bilangan pertama = ");
    scanf("%f", &bil1);
    
    printf("Masukan bilangan kedua = ");
    scanf("%f", &bil2);
    
    if (bil2 == 0) {
        printf("divide by zero");
    } else {
        hasil = bil1 / bil2;
        printf("%.3f dan %.3f = %.3f", bil1, bil2, hasil);
    }
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////     SOAL                                               //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

void soal3() {

    int nilai;

    printf("Masukan nilai = ");
    scanf("%d", &nilai);
    
    if (nilai < 0) {
        nilai *= -1;
    }
    
    printf("nilai = %d", nilai);

}

void soalA() {
    int bil;

    printf("Masukan bil = ");
    scanf("%d", &bil);
    
    if (bil > 0) {
        printf("Kategori A");
    }
}

void soalB() {
    int bil;

    printf("Masukan bil = ");
    scanf("%d", &bil);
    
    if (bil > 0) {
        printf("Kategori A");
    } else {
        printf("Kategori B");
    }
}

void soalC() {
     int bil;

    printf("Masukan bil = ");
    scanf("%d", &bil);
    
    if (bil > 0) {
        printf("Kategori A");
    } else {
        printf("Kategori B");
        printf("Kategori A");
    }
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////                        MAIN                            //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

int main() {

    pendahuluan2();

    return 0;
}
