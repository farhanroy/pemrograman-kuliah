#include <stdio.h>
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////     PENDAHULUAN                                        //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
void pendahuluan1() {
    int dollar;
    printf("Masukan nilai dollar = $ ");
    scanf("%d", &dollar);

    int rupiah = dollar*11090;
    printf("$%d = Rp.%d ", dollar, rupiah);
}

void pendahuluan2() {
    int sisa;
    int jumlahUang = 189000;

    int ratusanRibu = jumlahUang / 100000;
    sisa = jumlahUang - (ratusanRibu*100000);
    int limaPuluhRibuan = sisa / 50000;
    sisa -= limaPuluhRibuan*50000;
    int duaPuluhRibuan = sisa / 20000;
    sisa -= duaPuluhRibuan*20000;
    int sePuluhRibuan = sisa / 10000;
    sisa -= sePuluhRibuan*10000;
    int limaRibuan = sisa / 5000;
    sisa -= limaRibuan*5000;
    int duaRibuan = sisa / 2000;
    sisa -= duaRibuan*2000;
    int seRibuan = sisa / 1000;
    sisa -= seRibuan*1000;

    printf("%d", limaPuluhRibuan);


}

void pendahuluan3() {
   int a = 12, b = 2, c = 3, d = 4;
	
    printf("%d ",a % b);
	printf("\n%d", a - c);
	printf("\n%d", a + b);
	printf("\n%d", a / d);
	printf("\n%d", a / d * d + a % d);
	printf("\n%d", a % d / d * a - c);
}

void pendahuluan4() {
    int a, b, c;
    printf("Masukan nilai a = ");
    scanf("%d", &a);
    printf("Masukan nilai b = ");
    scanf("%d", &b);
    printf("Masukan nilai c = ");
    scanf("%d", &c);

    int diskriminan = (b*b) - 4*(a*c);
    printf("%d, %d, %d = %d", a, b, c, diskriminan);
}

void pendahuluan5() {

    float a = 12.0, b = 2.0, c = 3.0, d = 4.0;
	
    printf("%f ",a % b);
	printf("\n%f", a - c);
	printf("\n%f", a + b);
	printf("\n%f", a / d);
	printf("\n%f", a / d * d + a % d);
	printf("\n%f", a % d / d * a - c);
    
    // Pendahuluan 4
    float a, b, c;

    printf("Masukan nilai a = ");
    scanf("%f", &a);
    printf("Masukan nilai b = ");
    scanf("%f", &b);
    printf("Masukan nilai c = ");
    scanf("%f", &c);

    float diskriminan = (b*b) - 4*(a*c);
    printf("%f, %f, %f = %f", a, b, c, diskriminan);   
}




//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////     SOAL                                               //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

void soalA() {
    int a = 22;

    a = a + 5;
    a = a - 2;
    printf("a = %d\n", a); // Hasil: 25
}

void soalB() {
    int x;
    
    // 1
    x = (2 + 3) - 10 * 2;
    // 2
    x = (2 + 3) - (10 * 2);
    // 3
    x = 10 % 3 * 2 + 1;
    
    printf("Hasil dari x = %d", x);
}

void soalC() {
    
}

void soalD() {
    char kar = 'A';

    kar = kar + 32;
    printf("%c\n",kar);
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////                        MAIN                            //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

int main() {

    pendahuluan2();

    return 0;
}
