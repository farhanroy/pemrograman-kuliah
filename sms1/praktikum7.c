#include <stdio.h>
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////     PENDAHULUAN                                        //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

/*

*/
void pendahuluan1() {
       int angka;
    
    printf("Masukan angka = ");
    scanf("%d", &angka);
    switch(angka) {
        case 1:
            printf("Minggu");
            break;
        case 2:
            printf("Senin");
            break;
        case 3:
            printf("Selasa");
            break;
        case 4:
            printf("Rabu");
            break;
        case 5:
            printf("Kamis");
            break;
        case 6:
            printf("Jumat");
            break;
        case 7:
            printf("Sabtu");
            break;
        default:
            printf("Hari tidak valid");
            break;
    }

}

void pendahuluan2() {
     int validOperator = 1; //valid_operator diinisialisasi dengan logika 1
    char operasi;
    float number1, number2, result;
    
    printf("Masukkan 2 buah bilangan & sebuah operator\n");
    printf("dengan format : number1 operator number2\n\n");
    scanf("%f %c %f", &number1, &operasi, &number2);
    
    switch(operasi) {
        case '*':
            result = number1 * number2;
            break;
        case '/':
            result = number1 / number2;
            break;
        case '+':
            result = number1 + number2;
            break;
        case '-':
            result = number1 - number2;
                break;
        default:
            validOperator = 0;
            break;
    } 
    
    if(validOperator) {
        printf("\n%g %c %g is %g\n", number1, operasi, number2, result );
    }
    else {
        printf("Invalid operator!\n");
    }
    
}

void pendahuluan3() {
    int menu;  
    
    int sisiKubus, jariLingkaran, ariSilinder, tinggiSilinder;
    float volumeKubus, luasLingkaran, volumeSilinder;
    
    printf("Menu:\n1. Menghitung volume kubus\n2. Menghitung luas lingkaran\n3. Menghitung volume silinder.\nMasukan pilihan: ");
    scanf("%d", &menu);
    
    switch(menu) {
        case 1:
            
            printf("Masukan sisi kubus = ");
            scanf("%d", &sisiKubus);
            
            volumeKubus = sisiKubus*sisiKubus*sisiKubus;
            printf("Volume kubus = %d", volumeKubus);
            break;
        case 2:
            
            printf("Masukan jari jari lingkaran = ");
            scanf("%d", &jariLingkaran);
            
            luasLingkaran = (jariLingkaran*jariLingkaran)*3.14;
            printf("Volume kubus = %.3f", luasLingkaran);
            break;
        case 3:
            
            printf("Masukan jari jari silinder = ");
            scanf("%d", &jariSilinder);
            
            printf("Masukan tinggi silinder = ");
            scanf("%d", &tinggiSilinder);
            
            volumeSilinder = (jariSilinder*jariSilinder)*3.14*tinggiSilinder;
            printf("Volume silinder = %.3f", volumeSilinder);
            break;
        default:
            printf("inputan tidak valid");
            break;
    }

}



//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////     SOAL                                               //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

void soal3() {

    int nilai;

    printf("Masukan nilai = ");
    scanf("%d", &nilai);
    
    if (nilai < 0) {
        nilai *= -1;
    }
    
    printf("nilai = %d", nilai);

}

void soalA() {
    int bil;

    printf("Masukan bil = ");
    scanf("%d", &bil);
    
    if (bil > 0) {
        printf("Kategori A");
    }
}

void soalB() {
    int bil;

    printf("Masukan bil = ");
    scanf("%d", &bil);
    
    if (bil > 0) {
        printf("Kategori A");
    } else {
        printf("Kategori B");
    }
}

void soalC() {
     int bil;

    printf("Masukan bil = ");
    scanf("%d", &bil);
    
    if (bil > 0) {
        printf("Kategori A");
    } else {
        printf("Kategori B");
        printf("Kategori A");
    }
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////                        MAIN                            //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

int main() {

    pendahuluan2();

    return 0;
}
