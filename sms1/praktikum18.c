#include <stdio.h>

#define MAKS 2
// #define MAKS 999

void percobaan1();
void percobaan2();
void percobaan3();
void percobaan4();

int main() {
    
    return 0;
}

void percobaan1() {
    int arr[5] = {1,2,3,4,5};

    for(int n = 0; n < 5; n++) {
        printf("%d ", arr[n]);
    }
}
void percobaan2() {
    int i, n, k, fibo[47] = {0,1};
    
    printf("Masukkan n : ");
    scanf("%d", &n);
    printf("\nBarisan Fibonacci = ");

    for(i=2; fibo[i-2]+fibo[i-1]<=n; i++)
        fibo[i] = fibo[i-2] + fibo[i-1];
        
    for(k=0; k<i; k++)
        printf("%d ", fibo[k]);
}

void percobaan3() {
    int a[MAKS][MAKS], b[MAKS][MAKS], c[MAKS][MAKS];

    for(int i = 0; i < MAKS; i++) {
        for(int j = 0; j < MAKS; j++) {
            printf("Masukkan nilai matriks A [%d][%d] : ", i+1, j+1);
            scanf("%d", &a[i][j]);
        }
    }
    
    printf("\n");
    
    for(int i = 0; i < MAKS; i++) {
        for(int j = 0; j < MAKS; j++) {
            printf("Masukkan nilai matriks B[%d][%d] : ", i+1, j+1);
            scanf("%d", &b[i][j]);
        }
    }

    printf("Matriks A\n");
    for(int i = 0; i < MAKS; i++)
    {
        for(int j = 0; j < MAKS; j++)
        {
            printf("%3d ", a[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    
    printf("Matriks B\n");
    for(int i = 0; i < MAKS; i++) {
        for(int j = 0 ; j < MAKS; j++) {
            printf("%3d ", b[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    printf("Matriks C (A+B)\n");
    for(int i = 0; i < MAKS; i++)
    {
        for(int j = 0; j < MAKS; j++)
        {
            printf("%3d ", a[i][j]+b[i][j]);
        }
        printf("\n");
    }

}
void percobaan4() {
    int n, temp = 0, data[MAKS];
    
    printf("Masukkan jumlah data : ");
    scanf("%d", &n);

    for(int i = 0; i < n; i++) {
        printf("Masukkan indeks array ke-%d : ", i);
        scanf("%d", &data[i]);

        if(i == 0 || temp < data[i]) {
            temp = data[i];
        }
    }
    printf("\nNilai terbesar dari data diatas = %d", temp);
}