#include <stdio.h>
#include <string.h>

int bandingstr1(char [], char []);

int bandingstr2(char [], char []);

int main() {
    char x[30], y[30];
    
    printf("Masukan string x: ");
    fgets(x, sizeof x, stdin);

    printf("Masukan string y: ");
    fgets(y, sizeof y, stdin);

    if (bandingstr1(x, y) == 0) {
        printf("x dan y sama (CASE SENSITIVE) \n");
    } else {
        printf("x dan y tidak sama (CASE SENSITIVE) \n");
    }

    if (bandingstr2(x, y) == 0) {
        printf("x dan y sama (CASE INSENSITIVE)");
    } else {
        printf("x dan y tidak sama (CASE INSENSITIVE)");
    }

    return 0;
}

int bandingstr1(char x[], char y[]) {
   return strcmp(x, y); 
}

int bandingstr2(char x[], char y[]) {
   return strcmpi(x, y); 
}