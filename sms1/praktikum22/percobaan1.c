#include <stdio.h>
#include <string.h>

int main() {
    char asal[30], tujuan[30];
    
    printf("Masukan string asal: ");
    fgets(asal, sizeof asal, stdin);
    
    // Panjang String
    printf("Panjang string asal = %d ", strlen(asal));
    
    // NOTE: Just support in compiler Turbo
    // Membalikan String
    printf("\nKebalikan string asal = %s", strrev(asal));
    
    // Copy string
    strcpy(tujuan, asal);
    printf("\nPanjang string asal = %s ", tujuan);
    
    return 0;
}