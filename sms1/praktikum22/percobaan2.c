#include <stdio.h>
#include <string.h>

int bandingstr1(char [], char []);

int main() {
    char x[30], y[30];
    
    printf("Masukan string x: ");
    fgets(x, sizeof x, stdin);

    printf("Masukan string y: ");
    fgets(y, sizeof y, stdin);

    printf("%d\n", bandingstr1(x, y));
    printf("%d\n", bandingstr2(x, y));

    return 0;
}

int bandingstr1(char x[], char y[]) {
    int temp;
    for (int i = 0; i < strlen(x); i++) {
        if (x[i] != y[i]) {
            temp = 0;
            break;
        } else {
            temp = 1;
        }
    }
    
    return temp;
}