#include <stdio.h>
#include <string.h>

int bandingstr2(char [], char []);

int main() {
    char x[30], y[30];
    
    printf("Masukan string x: ");
    fgets(x, sizeof x, stdin);

    printf("Masukan string y: ");
    fgets(y, sizeof y, stdin);

    printf("%d\n", bandingstr2(x, y));

    return 0;
}

int bandingstr2(char x[], char y[]) {
    int temp = 0;
    for (int i = 0; i < strlen(x); i++) {
        if ((x[i] == y[i]) || ((x[i]+32 == y[i]) || (x[i]-32 == y[i]))){
            temp = 1;

        } else {
            temp = 0;
        }
    }
    
    return temp;
}