#include <stdio.h>
#include <string.h>

#define MAX 200

// Variabel untuk jumlah pesanan
int n;

struct order {
    char code;
    int amount;
};

void printListPrice();

void inputOrder(struct order []);
void outputOrder(struct order []);

int main() {
    struct order items[MAX];
    
    printListPrice();
    
    inputOrder(items);

    outputOrder(items);
    
    return 0;
}

void printListPrice() {
    printf("Toko Gerobak Fried Chicken\n\n");
    printf("DAFTAR HARGA\n");
    printf("Kode\tJenis\tHarga Per Potong\n");
    
    printf("D\tDada\tRp.5000,00\n");
    printf("P\tPaha\tRp.4000,00\n");
    printf("S\tSayap\tRp.3000,00\n");

    printf("\nHarga belum termasuk pajak : 10\n\n");
}

void inputOrder(struct order items[]) {
    printf("Masukan jumlah pesanan: ");
    fflush(stdin);
    scanf("%d", &n);
    
    for (int i = 0; i < n; i++) {
        printf("Data ke-%d\n", i+1);
        fflush(stdin);
        
        printf("Ukuran D/P/S: ");
        scanf("%c", &items[i].code);
        fflush(stdin);
        
        printf("Jumlah: ");
        scanf("%d", &items[i].amount);
        fflush(stdin);
        
        puts("");
    }
}

void outputOrder(struct order items[]) {
    int total = 0;
    printf("=============================================================\n");
    printf("No\tJenis Potong\tHarga Satuan\tQTY\tJumlah Harga\n");
    printf("=============================================================\n");
    for(int i = 0; i < n; i++) {
        char type[20];
        int harga;
        switch(items[i].code) {
            case 'D': case 'd':
                strcpy(type, "DADA");
                harga = 5000 * items[i].amount;
                break;
            case 'P': case 'p':
                strcpy(type, "PAHA");
                harga = 4000 * items[i].amount;
                break;
            case 'S': case 's':
                strcpy(type, "SAYAP");
                harga = 3000 * items[i].amount;
                break;
        }

        printf("%d\t%s\t\tRp.5000,00\t%d\t%d\n", i+1, type, items[i].amount, harga);
        total += harga;
    }

    printf("=============================================================\n");
    printf("Jumlah Bayar: Rp. %d\n", total);
    printf("Pajak PPN 10%%: Rp. %.0f\n", total*0.1);
    printf("Total Bayar: Rp. %.0f\n", total+(total*0.1));
    
}


