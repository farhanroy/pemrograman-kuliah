#include <stdio.h>
#include <stdlib.h>

struct date{
	int day, month, year;
};

struct date checkDate(struct date today);
int isAkhirBulan(int, int, int);
int isAkhirTahun();
int isKabisat(int);

int main() {
    char jawab = 'y';
    struct date today, tomorrow;
    
    while (jawab=='y' || jawab=='Y') {
        printf("Tanggal hari ini:");
        scanf("%d", &today.day);
        printf("Bulan saat ini:");
        scanf("%d", &today.month);
        printf("Tahun saat ini:");
        scanf("%d", &today.year);

        tomorrow = checkDate(today);

        printf("\n%d %d %d\n", tomorrow.day, tomorrow.month, tomorrow.year);
        
        fflush(stdin);
        printf("Mau Masukkan lagi [y/t] ? ");
        jawab = getchar();

    }
    return 0;
}

struct date checkDate(struct date today) {
    int day = today.day;
    int month = today.month;
    int year = today.year;

    if (isAkhirBulan(day, month, year)) {
        day = 1;
        month++;
    } else {
        day++;
    }

    if (isAkhirTahun(month)) {
        month = 1;
        year++;
    }

    struct date tomorrow = {day, month, year};
    return tomorrow;
}

int isAkhirTahun(int bulan) {
    return bulan == 13 ? 1 : 0;
}

int isAkhirBulan(int hari, int bulan, int tahun) {
    int jumlahHari[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    if (isKabisat(tahun)) {
        jumlahHari[2] = 29;
    }

    if (hari == jumlahHari[bulan]) {
        return 1;
    } else {
        return 0;
    }
}

int isKabisat(int tahun) {
    if (tahun%400 == 0){
        return 1;
    } else if(tahun%100 == 0){
        return 0;
    } else if(tahun%4 == 0){
        return 1;
    } else {
        return 0;
    }
}
