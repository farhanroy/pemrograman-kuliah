#include <stdio.h>
#include <stdlib.h>
int prima();

int main()
{
    int n, hasil;

    printf("Masukkan bilangan : ");
    scanf("%d", &n);

    hasil = prima(n);
    if(hasil==1)
        printf("%d adalah bilangan prima", n);
    else
        printf("%d bukan bilangan prima", n);
    return 0;
}

int prima(int n, int i = 2){
   if (n <= 2)
        return (n == 2) ? 1 : 0;
    if (n % i == 0)
        return 0;
    if (i * i > n)
        return 1;

    return isPrime(n, i + 1);    
}
