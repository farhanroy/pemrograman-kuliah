#include <stdio.h>
#include <stdlib.h>

float konversi(float,char,char);

int main()
{
    float suhu, hasil;
    char awal, tujuan;

    printf("Masukkan suhu sumber : ");
    scanf("%f", &suhu);
    getchar();

    printf("Masukkan satuan asal : ");
    scanf("%c", &awal);
    getchar();

    printf("Masukkan satuan tujuan : ");
    scanf("%c", &tujuan);
    getchar();
    
    hasil = konversi(suhu,awal,tujuan);
    
    printf("Hasil Konversi Suhu %f %c = %f %c", suhu, awal, hasil, tujuan);
    return 0;
}

float konversi(float x, char y, char z)
{
    float pembandingy, pembandingz, suhuawaly, suhuawalz, suhuz;

    switch(y)
    {
        case 'C': pembandingy = 5; suhuawaly = 0; break;
        case 'R': pembandingy = 4; suhuawaly = 0; break;
        case 'F': pembandingy = 9; suhuawaly = 32; break;
        default: break;
    }
    switch(z)
    {
        case 'C': pembandingz = 5; suhuawalz = 0; break;
        case 'R': pembandingz = 4; suhuawalz = 0; break;
        case 'F': pembandingz = 9; suhuawalz = 32; break;
        
    }
    suhuz = (pembandingz / pembandingy) * (x - suhuawaly) + suhuawalz;

    return suhuz;
}

