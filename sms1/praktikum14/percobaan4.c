#include <stdio.h>
#include <stdlib.h>

#define PI 3.14159f

float radian(float);

int main()
{
    float hasil, n;
    
    printf("Masukkan nilai derajat : ");
    scanf("%f", &n);

    hasil = radian(n);
    printf("Derajat dalam radian = %f", hasil);
    return 0;
}

float radian(float x)
{
    float rad;
    rad = (PI / 180.0f) * x;

    return rad;
}