#include <stdio.h>
#include <stdlib.h>
int faktorial();

int main()
{
    int n, total;

    printf("Masukkan bilangan : ");
    scanf("%d", &n);

    total = faktorial(n);
    printf("Hasil faktorialnya = %d", total);
    return 0;
}

int faktorial(int i) {

   if(i <= 1) {
      return 1;
   }
   return i * faktorial(i - 1);
}
