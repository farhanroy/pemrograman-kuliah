#include <stdio.h>

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////     PENDAHULUAN                                        //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

/*

*/
void pendahuluan1() {
   int n;
   printf("Masukan nilai n = ");
   scanf("%d", &n);

   for( int i = 1; i <= n; i++ ) {
       if ( i % 2 != 0 ) {
           printf("%d ", i);
       }
   }
}

void pendahuluan2() {
    int n;
    printf("Masukan nilai n = ");
    scanf("%d", &n);
   
    int temp = 0;
    for( int i = n; i >= 1; i-- ) {
        if (i == 1) {
            printf("1 = %d", temp);
            break;
        }
        printf("%d + ", i);
        temp += i;
    }
    
    
}

void pendahuluan3() {
    char a;
    for(a = 'Z'; a >= 'A'; a=a-1) {
        printf("%c \n", a); 
    }
}

void pendahuluan4() {
    int n;
    printf("Masukan nilai n = ");
    scanf("%d", &n);
   
    for( int i = 1; i <= n; i++ ) {
       if ( i % 2 == 0 ) {
           printf("%d ", i*-1);
       } else {
           printf("%d ", i);
       }
    }
}

void pendahuluan5() {
    int n;
    printf("Masukan nilai n = ");
    scanf("%d", &n);
   
    for(int i=2; i<= n; i++) {
        int isPrima = 1;
            
        for (int j = 2; j < i; j++) {
                if(i%j==0){
                    isPrima = 0;
                    break;
                } else {
                    break;
                }
        }
            if (i == n) {
                if(isPrima==1){
                    printf("Bilangan adalah bilangan prima");
                } else {
                    printf("Bilangan bukan bilangan prima");
                }
            
            }
        }

}

void pendahuluan6() {
    char huruf;
    int angka, jam, a, jmlJ=0, jmlNJ=0;
    float hasil=0;
    int A=4, B=3, C=2, D=1, E=0;
    for(a=1; a<=5; a++){
        printf("Jumlah jam dan Nilai mata kuliah %d : ", a);
        scanf("%d %c", &jam, &huruf);
        if(huruf == 'A'){
            angka = 4; 
            
        }
        else if(huruf == 'B') {
            angka = 3; 
            
        }
        else if(huruf == 'C'){
            angka = 2; 
            
        } else if(huruf == 'D') {
            angka = 1; 
            
        } else { angka = 0; } 
        jmlJ  = jmlJ + jam;
        jmlNJ = jmlNJ + (angka*jam);
        
    }
    hasil = jmlNJ / jmlJ;
    printf("Total nilai %d dan total jam %d maka hasilnya %.2f", jmlNJ, jmlJ, hasil);
    
}


//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////                        MAIN                            //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

int main() {

    pendahuluan6();

    return 0;
}
