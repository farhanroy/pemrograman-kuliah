#include <stdio.h>

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////     PENDAHULUAN                                        //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

/*

*/
void pendahuluan1() {
   char karakter;
    
    printf("Masukan karakter = ");
    
    
    while (karakter != 'X') {
        scanf("%c", &karakter);
    }
    
    printf("Maaf anda mengetikan huruf X");
}

void pendahuluan2() {
    char jawab='y';
    int temp=0, hasil=0, counter=0;

    while (jawab=='y')
    {
        counter++;

        printf("Masukkan bilangan ke-%d : ", counter);
        scanf("%d", &temp);

        printf("Mau Masukkan lagi [y/t] ? ");
        getchar();

        jawab = getchar();

        hasil += temp;
    }
    printf("Hasilnya = %d \n", hasil);    
}

void pendahuluan3() {
    char jawab='y';
    int temp = 0, hasil = 0, counter = 0, terkecil = 0, terbesar = 0;

    while (jawab=='y' || jawab=='Y')
    {
        counter++;

        printf("Masukkan bilangan ke-%d : ", counter);
        scanf("%d", &temp);
        
        terkecil = counter == 1 ? temp : terkecil;

        printf("Mau Masukkan lagi [y/t] ? ");
        getchar();

        jawab = getchar();

        hasil += temp;
        
        if (temp < terkecil) {
            terkecil = temp;
        } 
        if (temp > terbesar) {
            terbesar = temp;
        }
        
        
    }
    printf("Hasilnya = %d \n", hasil);

    printf("Rata rata = %d \n", hasil/counter);
    
    printf("Maksimum = %d \n", terbesar);
    
    printf("Minimum = %d \n", terkecil);
}

void pendahuluan4() {
    int temp = 0, counter = 0, n;
    
    printf("Masukkan n : ");
    scanf("%d", &n);

    while (temp <= n)
    {
        printf("%d ", temp);
        counter++;
        temp += counter;
    }
}

void pendahuluan5() {
    char karakter;
    int counter, jumlahKarakter = 0, jumlahSpasi = 0;
    
    printf("Masukan karakter = ");
    
    
    while ((karakter = getchar()) != '\n') {
        if (karakter == ' ') {
            jumlahSpasi++;
        } else {
            jumlahKarakter++;
        }
        
        counter++;
    }
    
    printf("Jumlah karakter %d\n", jumlahKarakter);
    printf("Jumlah spasi %d", jumlahSpasi);
}

void pendahuluan6() {
    int  
    
    printf("Masukkan n : ");
    scanf("%d", &n);
    
    while (n != 0) {
        digit = n % 10;
        n /= 10;
        if (n == 0) {
            printf("%d = ", digit);
        } else {
            printf("%d + ", digit);
        }
        
        hasil += digit;
    }    
    printf("%d", hasil);    
}

void pendahuluan7() {
    int temp = 0;
    int bulanan = 500000;
    int biaya = 25000000;
    int counter = 0;
    
    while(temp < biaya ) {
        
        temp += bulanan;
        if (temp > biaya) break;
        counter++;
        printf("%d , %d dan %d\n",counter, temp, biaya);
        if (counter % 12 == 0) {

            biaya += biaya * 0.04; 
        }
    }
    
    printf("%d", counter);
}

void soal3() {
    char karakter;
    int counter, jumlahAbjadKecil = 0, jumlahAbjadBesar = 0, jumlahAngka = 0, jumlahSpasi = 0;
    
    printf("Masukan karakter = ");
    
    
    while ((karakter = getchar()) != '\n') {
        if (karakter == ' ') {
            jumlahSpasi++;
        } else if (karakter >= 97 && karakter <= 122){
            jumlahAbjadKecil++;
        } else if (karakter >= 65 && karakter <= 90){
            jumlahAbjadBesar++;
        } else if (karakter >= 48 && karakter <= 57){
            jumlahAngka++;
        } else {
            //
        }
        
        counter++;
    }
    
    printf("Jumlah huruf kecil =  %d\n", jumlahAbjadKecil);
    printf("Jumlah huruf besar =  %d\n", jumlahAbjadBesar);
    printf("Jumlah angka =  %d\n", jumlahAngka);
    printf("Jumlah spasi %d", jumlahSpasi);
}

void soal4() {
     int temp = 0, bulanan, biaya, tiap6Bulan, counter = 0;
     float peningkatan;
    
    printf("Masukan biaya = ");
    scanf("%d", &biaya);
    
    printf("Masukan bulanan = ");
    scanf("%d", &bulanan);
    
    printf("Masukan tambahan tiap 6 bulan = ");
    scanf("%d", &tiap6Bulan);
    
    printf("Masukan peningkatan = ");
    scanf("%f", &peningkatan);
    
    peningkatan /= 100;
    
    while(temp < biaya ) {
        
        temp += bulanan;
        if (temp > biaya) break;
        counter++;
        
        if (counter % 12 == 0) {

            biaya += biaya * 0.04; 
        }
        
        if (counter % 6 == 0) {
            bulanan += tiap6Bulan;
        }
    }
    
    printf("%d", counter);
    
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////                        MAIN                            //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

int main() {

    pendahuluan7();

    return 0;
}
