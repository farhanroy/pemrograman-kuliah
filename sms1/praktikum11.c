#include <stdio.h>

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////     PENDAHULUAN                                        //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

/*

*/
void pendahuluan1() {
   char karakter;
   printf("Masukan karakter: ");

   do {
    scanf("%c", &karakter);
   } while(karakter != '\n');

    printf("Main Block");
}

void pendahuluan2() {
    int n;
    
    printf("Masukan bilangan-n: ");
    scanf("%d", &n);
    
    for (int i = 1; i <= n; i++) {
        if (i % 2 == 0 || i % 3 == 0) {
            continue;
        }
        printf("%d ", i);
    } 
}

void pendahuluan3() {
    int n;
    
    printf("Masukan bilangan-n: ");
    scanf("%d", &n);
    
    for (int i = 1; i <= n; i++) {
        if (i > 0 && i < 100) {
            break;
        }
        
        if (i % 2 == 0 || i % 7 == 0  || i % 11 == 0) {
            continue;
        }
        printf("%d ", i);
    }
    printf("\n program berakhir");
}

void pendahuluan4() {
    int i, n, temp, terbesar =0 , terkecil = 0, jumlah = 0;
    
    printf("Masukan jumlah data: ");
    scanf("%d", &n);
    
    for (i = 1; i <= n; i++) {
        printf("\n Masukan nilai ke-%d: ", i);
        scanf("%d", &temp);
        
        terkecil = i == 1 ? temp : terkecil;
        
        if (temp < terkecil) {
            terkecil = temp;
        } 
        if (temp > terbesar) {
            terbesar = temp;
        }
        
        jumlah += temp;
    }
    
    printf("\nterkecil = %d", terkecil);
    printf("\nterbesar = %d", terbesar);
    printf("\nrata-rata = %d", i);
    
}

void pendahuluan5() {
    int n;
    
    printf("Masukan jumlah data: ");
    scanf("%d", &n);

    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= n; j++) {
            printf("%d ", j);
        }   
        printf("\n");
    }
}

void pendahuluan6() {
    int n;
    
    printf("Masukan jumlah data: ");
    scanf("%d", &n);

    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= i; j++) {
            printf("%d ", i);
        }   
        printf("\n");
    }
    
}

void pendahuluan7() {}


//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////                        SOAL                            //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

void soal3() {
    int n;
    printf("Masukan nilai n = ");
    scanf("%d", &n);
   
    for(int i=2; i<= n; i++) {
        int isPrima = 1;
            
        for (int j = 2; j < i; j++) {
                if(i%j==0){
                    isPrima = 0;
                    break;
                } else {
                    break;
                }
        }
            if (i == n) {
                if(isPrima==1){
                    printf("Bilangan adalah bilangan prima");
                } else {
                    printf("Bilangan bukan bilangan prima");
                }
            
            }
    }
}



//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////                        MAIN                            //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

int main() {

    pendahuluan6();

    return 0;
}
