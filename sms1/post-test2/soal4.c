#include <stdio.h>
void rubah (int *, int, int*);
int main()
{
    int a,b,c,*d;
    a = 88;
    b = 77;
    c = 60;
    d = &c;
    printf("Nilai sebelum pemanggilan fungsi\n");
    printf("a = %d b = %d d = %d \n", a, b, *d);
    rubah(&a,b,d);
    printf("\nNilai setelah pemanggilan fungsi\n");
    printf("a = %d b = %d d = %d \n", a, b, *d);
    return 0;
}
void rubah(int *x, int y, int *z) {
    *x = *x + 10;
    y = y - 20;
    *z = *z / 30;
    printf("\nNilai di akhir fungsi rubah()\n");
    printf("x = %d y = %d z = %d\n", *x, y, *z);
}