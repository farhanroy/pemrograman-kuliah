#include <stdio.h>
#include <string.h>

struct nilai {
    char nama[50];
    float fisika;
    float matematika;
    float kimia;
    float biologi;
};

int main() {      
    struct nilai items[5] = {
        {"Andi", 8.4, 8.4 , 9.4, 6.4},
        {"Hasan", 7.6, 6.6 , 7.6, 8.6},
        {"Dudi", 5.4, 8.4 ,5.4, 8.4},
        {"Adib", 4.6, 6.6 , 8.6, 7.6},
        {"Rahma", 7.6, 6.6 , 6.6, 7.6},
    };

    for (int i = 0; i < 5; i++) {
        float total;
        total += items[i].fisika;
        total += items[i].matematika;
        total += items[i].biologi;
        total += items[i].kimia;

        printf("Rata rata %s = %.1f\n", items[i].nama, total/4);
        total = 0;
    }

    float totalA;
    for (int i = 0; i < 5; i++) {
        
        totalA += items[i].fisika;
    }

    printf("Rata rata fisika = %.1f\n", totalA/5);

float totalB;
    for (int i = 0; i < 5; i++) {
        
        totalB += items[i].matematika;

        
    }
    printf("Rata rata matematika = %.1f\n", totalB/5);

float totalC;
    for (int i = 0; i < 5; i++) {
        totalC += items[i].biologi;
        
    }
    printf("Rata rata biologi = %.1f\n", totalC/5);
float totalD;
    for (int i = 0; i < 5; i++) {
        totalD += items[i].kimia;
    }
    printf("Rata rata kimia = %.1f\n", totalD/5);

    
    return 0;
}  