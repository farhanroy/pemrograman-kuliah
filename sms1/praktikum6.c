#include <stdio.h>
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////     PENDAHULUAN                                        //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

/*

*/
void pendahuluan1() {
    int suhu;
    
    printf("Masukan suhu = ");
    scanf("%d", &suhu);
    
    if (suhu < 0) {
        printf("Benda berbentuk padat");
    }
    else if (suhu >= 0 && suhu <= 100) {
        printf("Benda berbentuk cair");
    }
    else {
        printf("Benda berbentuk gas");
    }
}

void pendahuluan2() {
    int tesAkademik, tesKetrampilan, tesPsikologi;
    
    printf("Masukan nilai tes akademik = ");
    scanf("%d", &tesAkademik);
    
    printf("Masukan nilai tes ketrampilan = ");
    scanf("%d", &tesKetrampilan);
    
    printf("Masukan tes psikologi = ");
    scanf("%d", &tesPsikologi);
    
    int rata = (tesAkademik + tesKetrampilan + tesPsikologi) / 3;
    
    if (rata >= 75) {
        if (tesAkademik > tesKetrampilan && tesAkademik > tesPsikologi) {
            printf("Diterima ditempatka di bagian administrasi");
        }
        else if (tesKetrampilan > tesAkademik && tesKetrampilan > tesPsikologi) {
            printf("Diterima ditempatka di bagian produksi");
        } 
        else {
            printf("Diterima ditempatka di bagian pemasaran");
        }
    }
    else {
        printf("Maaf anda belum dapat kami terima");
    }
    
}

void pendahuluan3() {
    int bil1, bil2, menu;
    
    printf("Masukan nilai bilangan pertama = ");
    scanf("%d", &bil1);
    
    printf("Masukan nilai bilangan kedua = ");
    scanf("%d", &bil2);
    
    printf("Menu Matematika \n 1. Penjumlahan \n 2. Pengurangan \n 3. Pembagian \n 4. Perkalian \n");
    
    printf("Masukan pilihan anda: ");
    scanf("%d", &menu);
    
    if (menu == 1) {
        printf("%d + %d = %d", bil1, bil2, bil1+bil2);
    }
    else if (menu == 2) {
        printf("%d - %d = %d", bil1, bil2, bil1-bil2);
    }
    else if (menu == 3) {
        printf("%d / %d = %d", bil1, bil2, bil1/bil2);
    }
    else if (menu == 4) {
        printf("%d * %d = %d", bil1, bil2, bil1*bil2);
    }
    else {
        printf("Tidak ada menu");
    }
    
}

void pendahuluan4() {

    int nilaiAngka;
    
    printf("Masukan nilai angka = ");
    scanf("%d", &nilaiAngka);
    
    if (nilaiAngka <= 40) {
        printf("Nilai huruf adalah E");
    }
    else if (nilaiAngka > 40 && nilaiAngka <= 55) {
        printf("Nilai huruf adalah D");
    }
    else if (nilaiAngka > 55 && nilaiAngka <= 60) {
        printf("Nilai huruf adalah C");
    }
    else if (nilaiAngka > 60 && nilaiAngka <= 80) {
        printf("Nilai huruf adalah B");
    }
    else if (nilaiAngka > 80 && nilaiAngka <= 100) {
        printf("Nilai huruf adalah A");
    }
    else {
        printf("Tidak ada huruf dengan nilai itu");
    }


}

void pendahuluan5() {
    int angka;
    
    printf("Masukan angka = ");
    scanf("%d", &angka);
    
    if (angka == 1) {
        printf("Minggu");
    }
    else if (angka == 2) {
        printf("Senin");
    }
    else if (angka == 3) {
        printf("Selasa");
    }
    else if (angka == 4) {
        printf("Rabu");
    }
    else if (angka == 5) {
        printf("Kamis");
    }
    else if (angka == 6) {
        printf("Jumat");
    }
    else if (angka == 7) {
        printf("Sabtu");
    }
    else {
        printf("Hari tidak valid");
    }
    
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////     SOAL                                               //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

void soal3() {

     int angka;
    
    printf("Masukan angka = ");
    scanf("%d", &angka);
    
    if (angka == 1) {
        printf("Minggu");
    }
    else if (angka == 2) {
        printf("Senin");
    }
    else if (angka == 3) {
        printf("Selasa");
    }
    else if (angka == 4) {
        printf("Rabu");
    }
    else if (angka == 5) {
        printf("Kamis");
    }
    else if (angka == 6) {
        printf("Jumat");
    }
    else if (angka == 7) {
        printf("Sabtu");
    }
    else {
        printf("Hari tidak valid");
    }
    

}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////                        MAIN                            //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

int main() {

    pendahuluan2();

    return 0;
}
