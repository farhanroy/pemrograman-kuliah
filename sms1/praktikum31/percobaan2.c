#include <stdio.h>

#define MAX 200

int n;

typedef struct {
    int day;
    int month;
    int year;
} Date;

typedef struct {
    char name[20];
    Date birtday;
    char kelamin;
    int fee;
} Person;

void inputPerson(Person *person);
void outputPerson(Person *person);

int main() {
    Person person[30];

    printf("\nBerapa jumlah pegawai? ");
	scanf("%d",&n);

    inputPerson(person);
    outputPerson(person);

    return 0;
}


for(int i = 0; j < n-1; i++) {

    int currentMin = j;

    for(int j = i+1; i < n; i++) {

        if(arrayList[j] < arrayList[currentMin])
            currentMin = j;
    }

    swap(arrayList[i], arrayList[currentMin]);
}
   

void inputPerson(Person *person) {
    for (int i = 0; i < n; i++) {
        printf("\n\nData pegawai ke-%d\n", i+1);
        fflush(stdin);

        printf("Nama: ");
        scanf("%s", &(*person).name);
        fflush(stdin);

        printf("Tanggal lahir: ");
        scanf("%d-%d-%d", &(*person).birtday.day, &(*person).birtday.month, &(*person).birtday.year);
        fflush(stdin);

        printf("Jenis kelamin L/P: ");
        scanf("%c", &(*person).kelamin);
        fflush(stdin);

        printf("Gaji/bln: ");
        scanf("%d", &(*person).fee);
        fflush(stdin);

        person++;
    }

    printf("\n\n");

}

void outputPerson(Person *person) {

    printf("Data pegawai yang sudah diiinputkan \n\n");
    for (int i = 0; i < n; i++) {
        printf("No.id: %d\n", i+1);
        printf("Nama: %s\n", (*person).name);
        printf("Tanggal lahir: %d-%d-%d\n", (*person).birtday.day, (*person).birtday.month, (*person).birtday.year);
        if ((*person).kelamin == 'L')
            printf("Jenis kelamin: Laki-laki\n");
        else
            printf("Jenis kelamin: Perempuan\n");

        printf("Gaji/bln: %d", (*person).fee);

        person++;

        printf("\n\n");
    }
}