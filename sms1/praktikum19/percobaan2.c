#include <stdio.h>

int main() {
    int nilai[3][3] = {
        {81, 90, 62},
        {50, 83, 87},
        {77, 70, 92}
    };

    int data[3][2];
    
    printf("No.Mhs \t Rata\n");
    printf("-------------------\n");

    for(int i = 0; i < 3; i++) {
        int temp = 0;
        for(int j = 0; j < 3; j++) {
            temp += nilai[i][j];
        }
        data[i][2] = temp/3;
        
        printf("%d \t\t %d \n", i+1, data[i][2]);
    }
    printf("-------------------\n");

    return 0;
}