#include <stdio.h>

#define MAX 20

int jumlah;

void input(int []);
void findMax(int []);

int jumlah;

int main() {
    int data[MAX];
    
    input(data);
    findMax(data);
    
    return 0;
}

void input(int x[]) {
    int i;
    
    printf("Jumlah data = ");
    scanf("%d", &jumlah);
    
    for(i=0; i<jumlah; i++) {
        printf("Data ke-%d : ", i+1);
        scanf("%d", &x[i]);
    }
    puts("");
}

void findMax(int x[]) {
    int i, temp;
    
    for(i=0; i<jumlah; i++) {
        if(i == 0 || temp < x[i]) {
            temp = x[i];
        }
    }
    
    printf("Yang terbesar adalah %d", temp);
}