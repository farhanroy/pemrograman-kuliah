#include <stdio.h>

float f_to_i(float);
float i_to_cm(float);
float cm_to_m(float);

int main()
{
    float kaki, inchi, cm, m;
    
    printf("Masukkan ukuran (feet) : ");
    scanf("%f", &kaki);

    inchi = f_to_i(kaki);
    cm = i_to_cm(inchi);
    m = cm_to_m(cm);

    printf("%g feet = %g inchi\n", kaki, inchi);
    printf("%g inchi = %g cm\n", inchi, cm);
    printf("%g cm = %g m\n", cm, m);
    return 0;
}

float f_to_i(float x) {
    return x*12;
}

float i_to_cm(float a) {
    return a * 2.54;
}

float cm_to_m(float c) {
    return c * 100;
}