#include <stdio.h>

float input(int);
float average(int, float);

int main() {
    int n;
    float total,rata;

    printf("Masukkan jumlah data yang diinputkan : ");
    scanf("%d", &n);

    total = input(n);
    rata = average(n, total);
    
    printf("\nTotal dari seluruh data = %g", total);
    printf("\nRata-rata dari seluruh data = %g ", rata);
    return 0;
}

float input(int data) {
    int x;
    float jumlah=0, nilai;

    for(x=1; x<=data; x++) {
        printf("Masukkan data ke-%d : ", x);
        scanf("%f", &nilai);
        fflush(stdin);
        jumlah = jumlah + nilai;
    }
    return jumlah;
}

float average(int a, float sum) {
    return sum / a;

}
