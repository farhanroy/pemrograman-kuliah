#include <stdio.h>

int main() {
   char scan[3];
   printf("1. Input scanf: ");
   scanf("%s", scan);
   printf("nilai = %s\n",scan);
   fflush(stdin);
   
   printf("2. Input gets: \n");
   
   gets(scan);
   printf("nilai = %s \n",scan);
   
   printf("3. Input fgets: \n");
   
   fgets(scan, sizeof scan, stdin);
   printf("nilai = %s",scan);
   
    
    return 0;
}