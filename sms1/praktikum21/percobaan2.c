#include <stdio.h>
#include <string.h>

int hitungStr(char []);

int main() {
    char str[] = "HelloWorld";
    printf("Jumlah karakter = %d", hitungStr(str));
    return 0;
}

int hitungStr(char str[]) {
    int i = 0;
    while(str[i] != "\0") {
        i++;
    }
    return i;
}