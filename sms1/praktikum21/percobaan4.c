#include <stdio.h>

void copyStr(char [], char []);

int main() {
    char asal[30], tujuan[30];
    
    printf("Masukan string asal: ");
    fgets(asal, sizeof asal, stdin);
    
    copyStr(asal, tujuan);
    
    printf("String asal: %s", asal);
    printf("String tujuan: %s", tujuan);
    
    return 0;
}

void copyStr(char asal[], char tujuan[]) {
    int i = 0;
    
    while (asal[i] != '\0') {
        tujuan[i] = asal[i];
        i++;
    }
    
}

