#include <stdio.h>
#include <string.h>

int hitungStr(char []);

void balikStr(char []);

int main() {
    char str[] = "HelloWorld";
    balikStr(str);
    return 0;
}

int hitungStr(char str[]) {
    return strlen(str);
}

void balikStr(char str[]) {
    int panjang = hitungStr(str);
    int j = 0;
    char x[panjang];

    for(int i = panjang; i >= 0; i--) {
        x[j] = str[i];
        printf("%c", x[j]);
        j++; 
    }
}