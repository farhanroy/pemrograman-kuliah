#include <stdio.h>
#include <stdlib.h>

float kuadrat(float);
int main()
{
    float bil;

    printf("Masukkan angka : ");
    scanf("%f", &bil);

    printf("Nilai kuadrat dari %g = %g", bil, kuadrat(bil));
    return 0;
}

float kuadrat(float x) {
    return x * x;
}