#include <stdio.h>
#include <stdlib.h>

void triangular();

int main()
{
    int n;

    printf("Masukkan bilangan : ");
    scanf("%d", &n);

    triangular(n);
    return 0;
}

void triangular(int n)
{
    int total = 0;
    for(int i = n; n >= 1; n--) {
        if (n == 1) {
            printf("%d = ", n);
            break;
        } else {
            printf("%d + ", n);
        }
        total += n;
    }

    printf("%d", total);
}
