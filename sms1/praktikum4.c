#include <stdio.h>
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////     PENDAHULUAN                                        //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

/*

*/
void pendahuluan1() {
    float celcius;

    printf("Masukan celcius = ");
    scanf("%f", &celcius);

    float fehreinhet = celcius * 1.8 + 32;
    printf("%.3f C = %.3f F", celcius, fehreinhet);
}

void pendahuluan2() {
	    int uang, gajiPokok, lamaKerja;
    
    printf("Masukan gaji pokok = ");
    scanf("%d", &gajiPokok);
    
    printf("Masukan lama kerja (bulan) = ");
    scanf("%d", &lamaKerja);

    int tunjanganSuamiIstri = gajiPokok * 0.10;
    printf("%d\n", tunjanganSuamiIstri);

    int tunjanganAnak = gajiPokok * 0.05;
    printf("%d\n", tunjanganAnak);

    int bantuanTransport =  3000 * 30;
    printf("%d\n", bantuanTransport);
    
    int thr = (lamaKerja / 12) * 5000;
    printf("%d\n", thr);

    int pajak = 0.15 * (gajiPokok + tunjanganAnak + tunjanganSuamiIstri);
    printf("%d\n", pajak);

    int polisAsuransi = 20000;
    
    uang = gajiPokok + tunjanganSuamiIstri + tunjanganAnak + bantuanTransport + thr - pajak - polisAsuransi;

    printf("Gaji perbulan = %d", uang);

}

void pendahuluan3() {
    int jumlahTiket = 23;
	int jm3 = jumlahTiket / 3;
	int sm3 = jumlahTiket % 3;

	int harga = jm3 * (2 * 50000) + sm3 * 50000;
	printf("%d", harga);
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////     SOAL                                               //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

void soalA() {

    /*
- Variabel bisa terdiri dari huruf, angka dan karakter underscore / garis bawah ( _ ).
- Karakter pertama dari variabel hanya boleh berupa huruf dan underscore ( _ ), tidak bisa berupa angka. Meskipun dibolehkan, sebaiknya tidak menggunakan karakter underscore sebagai awal dari variabel karena bisa bentrok dengan beberapa variabel settingan program.
- Variabel harus selain dari keyword. Sebagai contoh, kita tidak bisa memakai kata int sebagai nama variabel, karena int merupakan keyword untuk menandakan tipe data integer.
- Beberapa compiler bahasa C ada yang membatasi panjang variabel maksimal 31 karakter. Agar lebih aman, sebaiknya tidak menulis nama variabel yang lebih dari 31 karakter.
    */

}

void soalB() {
    char c, d;
    c = 'd';
    d = c;
    printf("d = %c", d);
}

void soalC() {
    int x;

    printf("Masukan nilai x = ");
    scanf("%d", &x);

    int hasil = (3*(x*x))- (5*x) + 6;
	printf("hasil = %d ", hasil);
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////                                                        //////////////
////////////                        MAIN                            //////////////
////////////                                                        //////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

int main() {

    pendahuluan2();

    return 0;
}
